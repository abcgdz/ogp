package af.web.rest;

import af.domain.Duree;
import af.repository.DureeRepository;
import af.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link af.domain.Duree}.
 */
@RestController
@RequestMapping("/api")
public class DureeResource {

    private final Logger log = LoggerFactory.getLogger(DureeResource.class);

    private static final String ENTITY_NAME = "duree";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final DureeRepository dureeRepository;

    public DureeResource(DureeRepository dureeRepository) {
        this.dureeRepository = dureeRepository;
    }

    /**
     * {@code POST  /durees} : Create a new duree.
     *
     * @param duree the duree to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new duree, or with status {@code 400 (Bad Request)} if the duree has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/durees")
    public ResponseEntity<Duree> createDuree(@RequestBody Duree duree) throws URISyntaxException {
        log.debug("REST request to save Duree : {}", duree);
        if (duree.getId() != null) {
            throw new BadRequestAlertException("A new duree cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Duree result = dureeRepository.save(duree);
        return ResponseEntity.created(new URI("/api/durees/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /durees} : Updates an existing duree.
     *
     * @param duree the duree to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated duree,
     * or with status {@code 400 (Bad Request)} if the duree is not valid,
     * or with status {@code 500 (Internal Server Error)} if the duree couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/durees")
    public ResponseEntity<Duree> updateDuree(@RequestBody Duree duree) throws URISyntaxException {
        log.debug("REST request to update Duree : {}", duree);
        if (duree.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Duree result = dureeRepository.save(duree);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, duree.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /durees} : get all the durees.
     *

     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of durees in body.
     */
    @GetMapping("/durees")
    public List<Duree> getAllDurees() {
        log.debug("REST request to get all Durees");
        return dureeRepository.findAll();
    }

    /**
     * {@code GET  /durees/:id} : get the "id" duree.
     *
     * @param id the id of the duree to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the duree, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/durees/{id}")
    public ResponseEntity<Duree> getDuree(@PathVariable Long id) {
        log.debug("REST request to get Duree : {}", id);
        Optional<Duree> duree = dureeRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(duree);
    }

    /**
     * {@code DELETE  /durees/:id} : delete the "id" duree.
     *
     * @param id the id of the duree to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/durees/{id}")
    public ResponseEntity<Void> deleteDuree(@PathVariable Long id) {
        log.debug("REST request to delete Duree : {}", id);
        dureeRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
