package af.web.rest;

import af.domain.TypeDeProjet;
import af.repository.TypeDeProjetRepository;
import af.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link af.domain.TypeDeProjet}.
 */
@RestController
@RequestMapping("/api")
public class TypeDeProjetResource {

    private final Logger log = LoggerFactory.getLogger(TypeDeProjetResource.class);

    private static final String ENTITY_NAME = "typeDeProjet";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final TypeDeProjetRepository typeDeProjetRepository;

    public TypeDeProjetResource(TypeDeProjetRepository typeDeProjetRepository) {
        this.typeDeProjetRepository = typeDeProjetRepository;
    }

    /**
     * {@code POST  /type-de-projets} : Create a new typeDeProjet.
     *
     * @param typeDeProjet the typeDeProjet to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new typeDeProjet, or with status {@code 400 (Bad Request)} if the typeDeProjet has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/type-de-projets")
    public ResponseEntity<TypeDeProjet> createTypeDeProjet(@RequestBody TypeDeProjet typeDeProjet) throws URISyntaxException {
        log.debug("REST request to save TypeDeProjet : {}", typeDeProjet);
        if (typeDeProjet.getId() != null) {
            throw new BadRequestAlertException("A new typeDeProjet cannot already have an ID", ENTITY_NAME, "idexists");
        }
        TypeDeProjet result = typeDeProjetRepository.save(typeDeProjet);
        return ResponseEntity.created(new URI("/api/type-de-projets/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /type-de-projets} : Updates an existing typeDeProjet.
     *
     * @param typeDeProjet the typeDeProjet to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated typeDeProjet,
     * or with status {@code 400 (Bad Request)} if the typeDeProjet is not valid,
     * or with status {@code 500 (Internal Server Error)} if the typeDeProjet couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/type-de-projets")
    public ResponseEntity<TypeDeProjet> updateTypeDeProjet(@RequestBody TypeDeProjet typeDeProjet) throws URISyntaxException {
        log.debug("REST request to update TypeDeProjet : {}", typeDeProjet);
        if (typeDeProjet.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        TypeDeProjet result = typeDeProjetRepository.save(typeDeProjet);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, typeDeProjet.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /type-de-projets} : get all the typeDeProjets.
     *

     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of typeDeProjets in body.
     */
    @GetMapping("/type-de-projets")
    public List<TypeDeProjet> getAllTypeDeProjets() {
        log.debug("REST request to get all TypeDeProjets");
        return typeDeProjetRepository.findAll();
    }

    /**
     * {@code GET  /type-de-projets/:id} : get the "id" typeDeProjet.
     *
     * @param id the id of the typeDeProjet to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the typeDeProjet, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/type-de-projets/{id}")
    public ResponseEntity<TypeDeProjet> getTypeDeProjet(@PathVariable Long id) {
        log.debug("REST request to get TypeDeProjet : {}", id);
        Optional<TypeDeProjet> typeDeProjet = typeDeProjetRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(typeDeProjet);
    }

    /**
     * {@code DELETE  /type-de-projets/:id} : delete the "id" typeDeProjet.
     *
     * @param id the id of the typeDeProjet to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/type-de-projets/{id}")
    public ResponseEntity<Void> deleteTypeDeProjet(@PathVariable Long id) {
        log.debug("REST request to delete TypeDeProjet : {}", id);
        typeDeProjetRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
