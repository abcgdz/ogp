package af.web.rest;

import af.domain.BU;
import af.repository.BURepository;
import af.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link af.domain.BU}.
 */
@RestController
@RequestMapping("/api")
public class BUResource {

    private final Logger log = LoggerFactory.getLogger(BUResource.class);

    private static final String ENTITY_NAME = "bU";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final BURepository bURepository;

    public BUResource(BURepository bURepository) {
        this.bURepository = bURepository;
    }

    /**
     * {@code POST  /bus} : Create a new bU.
     *
     * @param bU the bU to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new bU, or with status {@code 400 (Bad Request)} if the bU has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/bus")
    public ResponseEntity<BU> createBU(@RequestBody BU bU) throws URISyntaxException {
        log.debug("REST request to save BU : {}", bU);
        if (bU.getId() != null) {
            throw new BadRequestAlertException("A new bU cannot already have an ID", ENTITY_NAME, "idexists");
        }
        BU result = bURepository.save(bU);
        return ResponseEntity.created(new URI("/api/bus/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /bus} : Updates an existing bU.
     *
     * @param bU the bU to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated bU,
     * or with status {@code 400 (Bad Request)} if the bU is not valid,
     * or with status {@code 500 (Internal Server Error)} if the bU couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/bus")
    public ResponseEntity<BU> updateBU(@RequestBody BU bU) throws URISyntaxException {
        log.debug("REST request to update BU : {}", bU);
        if (bU.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        BU result = bURepository.save(bU);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, bU.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /bus} : get all the bUS.
     *

     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of bUS in body.
     */
    @GetMapping("/bus")
    public List<BU> getAllBUS() {
        log.debug("REST request to get all BUS");
        return bURepository.findAll();
    }

    /**
     * {@code GET  /bus/:id} : get the "id" bU.
     *
     * @param id the id of the bU to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the bU, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/bus/{id}")
    public ResponseEntity<BU> getBU(@PathVariable Long id) {
        log.debug("REST request to get BU : {}", id);
        Optional<BU> bU = bURepository.findById(id);
        return ResponseUtil.wrapOrNotFound(bU);
    }

    /**
     * {@code DELETE  /bus/:id} : delete the "id" bU.
     *
     * @param id the id of the bU to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/bus/{id}")
    public ResponseEntity<Void> deleteBU(@PathVariable Long id) {
        log.debug("REST request to delete BU : {}", id);
        bURepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
