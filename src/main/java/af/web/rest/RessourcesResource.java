package af.web.rest;

import af.domain.Ressources;
import af.repository.RessourcesRepository;
import af.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link af.domain.Ressources}.
 */
@RestController
@RequestMapping("/api")
public class RessourcesResource {

    private final Logger log = LoggerFactory.getLogger(RessourcesResource.class);

    private static final String ENTITY_NAME = "ressources";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final RessourcesRepository ressourcesRepository;

    public RessourcesResource(RessourcesRepository ressourcesRepository) {
        this.ressourcesRepository = ressourcesRepository;
    }

    /**
     * {@code POST  /ressources} : Create a new ressources.
     *
     * @param ressources the ressources to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new ressources, or with status {@code 400 (Bad Request)} if the ressources has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/ressources")
    public ResponseEntity<Ressources> createRessources(@RequestBody Ressources ressources) throws URISyntaxException {
        log.debug("REST request to save Ressources : {}", ressources);
        if (ressources.getId() != null) {
            throw new BadRequestAlertException("A new ressources cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Ressources result = ressourcesRepository.save(ressources);
        return ResponseEntity.created(new URI("/api/ressources/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /ressources} : Updates an existing ressources.
     *
     * @param ressources the ressources to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated ressources,
     * or with status {@code 400 (Bad Request)} if the ressources is not valid,
     * or with status {@code 500 (Internal Server Error)} if the ressources couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/ressources")
    public ResponseEntity<Ressources> updateRessources(@RequestBody Ressources ressources) throws URISyntaxException {
        log.debug("REST request to update Ressources : {}", ressources);
        if (ressources.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Ressources result = ressourcesRepository.save(ressources);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, ressources.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /ressources} : get all the ressources.
     *

     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of ressources in body.
     */
    @GetMapping("/ressources")
    public List<Ressources> getAllRessources() {
        log.debug("REST request to get all Ressources");
        return ressourcesRepository.findAll();
    }

    /**
     * {@code GET  /ressources/:id} : get the "id" ressources.
     *
     * @param id the id of the ressources to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the ressources, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/ressources/{id}")
    public ResponseEntity<Ressources> getRessources(@PathVariable Long id) {
        log.debug("REST request to get Ressources : {}", id);
        Optional<Ressources> ressources = ressourcesRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(ressources);
    }

    /**
     * {@code DELETE  /ressources/:id} : delete the "id" ressources.
     *
     * @param id the id of the ressources to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/ressources/{id}")
    public ResponseEntity<Void> deleteRessources(@PathVariable Long id) {
        log.debug("REST request to delete Ressources : {}", id);
        ressourcesRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
