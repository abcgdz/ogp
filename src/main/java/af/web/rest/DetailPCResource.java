package af.web.rest;

import af.domain.DetailPC;
import af.repository.DetailPCRepository;
import af.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link af.domain.DetailPC}.
 */
@RestController
@RequestMapping("/api")
public class DetailPCResource {

    private final Logger log = LoggerFactory.getLogger(DetailPCResource.class);

    private static final String ENTITY_NAME = "detailPC";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final DetailPCRepository detailPCRepository;

    public DetailPCResource(DetailPCRepository detailPCRepository) {
        this.detailPCRepository = detailPCRepository;
    }

    /**
     * {@code POST  /detail-pcs} : Create a new detailPC.
     *
     * @param detailPC the detailPC to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new detailPC, or with status {@code 400 (Bad Request)} if the detailPC has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/detail-pcs")
    public ResponseEntity<DetailPC> createDetailPC(@RequestBody DetailPC detailPC) throws URISyntaxException {
        log.debug("REST request to save DetailPC : {}", detailPC);
        if (detailPC.getId() != null) {
            throw new BadRequestAlertException("A new detailPC cannot already have an ID", ENTITY_NAME, "idexists");
        }
        DetailPC result = detailPCRepository.save(detailPC);
        return ResponseEntity.created(new URI("/api/detail-pcs/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /detail-pcs} : Updates an existing detailPC.
     *
     * @param detailPC the detailPC to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated detailPC,
     * or with status {@code 400 (Bad Request)} if the detailPC is not valid,
     * or with status {@code 500 (Internal Server Error)} if the detailPC couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/detail-pcs")
    public ResponseEntity<DetailPC> updateDetailPC(@RequestBody DetailPC detailPC) throws URISyntaxException {
        log.debug("REST request to update DetailPC : {}", detailPC);
        if (detailPC.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        DetailPC result = detailPCRepository.save(detailPC);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, detailPC.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /detail-pcs} : get all the detailPCS.
     *

     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of detailPCS in body.
     */
    @GetMapping("/detail-pcs")
    public List<DetailPC> getAllDetailPCS() {
        log.debug("REST request to get all DetailPCS");
        return detailPCRepository.findAll();
    }

    /**
     * {@code GET  /detail-pcs/:id} : get the "id" detailPC.
     *
     * @param id the id of the detailPC to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the detailPC, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/detail-pcs/{id}")
    public ResponseEntity<DetailPC> getDetailPC(@PathVariable Long id) {
        log.debug("REST request to get DetailPC : {}", id);
        Optional<DetailPC> detailPC = detailPCRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(detailPC);
    }

    /**
     * {@code DELETE  /detail-pcs/:id} : delete the "id" detailPC.
     *
     * @param id the id of the detailPC to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/detail-pcs/{id}")
    public ResponseEntity<Void> deleteDetailPC(@PathVariable Long id) {
        log.debug("REST request to delete DetailPC : {}", id);
        detailPCRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
