/**
 * View Models used by Spring MVC REST controllers.
 */
package af.web.rest.vm;
