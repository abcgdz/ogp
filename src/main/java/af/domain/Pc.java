package af.domain;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;

import java.io.Serializable;

/**
 * A Pc.
 */
@Entity
@Table(name = "pc")
public class Pc implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "libelle")
    private String libelle;

    @Column(name = "code")
    private Integer code;

    @ManyToOne
    @JsonIgnoreProperties("pcs")
    private DetailPC detailPC;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLibelle() {
        return libelle;
    }

    public Pc libelle(String libelle) {
        this.libelle = libelle;
        return this;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public Integer getCode() {
        return code;
    }

    public Pc code(Integer code) {
        this.code = code;
        return this;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public DetailPC getDetailPC() {
        return detailPC;
    }

    public Pc detailPC(DetailPC detailPC) {
        this.detailPC = detailPC;
        return this;
    }

    public void setDetailPC(DetailPC detailPC) {
        this.detailPC = detailPC;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Pc)) {
            return false;
        }
        return id != null && id.equals(((Pc) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Pc{" +
            "id=" + getId() +
            ", libelle='" + getLibelle() + "'" +
            ", code=" + getCode() +
            "}";
    }
}
