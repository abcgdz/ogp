package af.domain;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * A BU.
 */
@Entity
@Table(name = "bu")
public class BU implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "nom")
    private String nom;

    @Column(name = "prenom")
    private String prenom;

    @OneToOne
    @JoinColumn(unique = true)
    private Projet l;

    @OneToMany(mappedBy = "bU")
    private Set<Ressources> dscs = new HashSet<>();

    @ManyToOne
    @JsonIgnoreProperties("bUS")
    private DetailPC detailPC;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public BU nom(String nom) {
        this.nom = nom;
        return this;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public BU prenom(String prenom) {
        this.prenom = prenom;
        return this;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public Projet getL() {
        return l;
    }

    public BU l(Projet projet) {
        this.l = projet;
        return this;
    }

    public void setL(Projet projet) {
        this.l = projet;
    }

    public Set<Ressources> getDscs() {
        return dscs;
    }

    public BU dscs(Set<Ressources> ressources) {
        this.dscs = ressources;
        return this;
    }

    public BU addDsc(Ressources ressources) {
        this.dscs.add(ressources);
        ressources.setBU(this);
        return this;
    }

    public BU removeDsc(Ressources ressources) {
        this.dscs.remove(ressources);
        ressources.setBU(null);
        return this;
    }

    public void setDscs(Set<Ressources> ressources) {
        this.dscs = ressources;
    }

    public DetailPC getDetailPC() {
        return detailPC;
    }

    public BU detailPC(DetailPC detailPC) {
        this.detailPC = detailPC;
        return this;
    }

    public void setDetailPC(DetailPC detailPC) {
        this.detailPC = detailPC;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof BU)) {
            return false;
        }
        return id != null && id.equals(((BU) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "BU{" +
            "id=" + getId() +
            ", nom='" + getNom() + "'" +
            ", prenom='" + getPrenom() + "'" +
            "}";
    }
}
