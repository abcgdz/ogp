package af.domain;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * A Ressources.
 */
@Entity
@Table(name = "ressources")
public class Ressources implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "nom")
    private String nom;

    @Column(name = "prenom")
    private String prenom;

    @Column(name = "acronyme")
    private String acronyme;

    @ManyToOne
    @JsonIgnoreProperties("ressources")
    private BU bU;

    @OneToMany(mappedBy = "ressources")
    private Set<Statut> ties = new HashSet<>();

    @OneToMany(mappedBy = "ressources")
    private Set<Fonction> svs = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public Ressources nom(String nom) {
        this.nom = nom;
        return this;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public Ressources prenom(String prenom) {
        this.prenom = prenom;
        return this;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getAcronyme() {
        return acronyme;
    }

    public Ressources acronyme(String acronyme) {
        this.acronyme = acronyme;
        return this;
    }

    public void setAcronyme(String acronyme) {
        this.acronyme = acronyme;
    }

    public BU getBU() {
        return bU;
    }

    public Ressources bU(BU bU) {
        this.bU = bU;
        return this;
    }

    public void setBU(BU bU) {
        this.bU = bU;
    }

    public Set<Statut> getTies() {
        return ties;
    }

    public Ressources ties(Set<Statut> statuts) {
        this.ties = statuts;
        return this;
    }

    public Ressources addTy(Statut statut) {
        this.ties.add(statut);
        statut.setRessources(this);
        return this;
    }

    public Ressources removeTy(Statut statut) {
        this.ties.remove(statut);
        statut.setRessources(null);
        return this;
    }

    public void setTies(Set<Statut> statuts) {
        this.ties = statuts;
    }

    public Set<Fonction> getSvs() {
        return svs;
    }

    public Ressources svs(Set<Fonction> fonctions) {
        this.svs = fonctions;
        return this;
    }

    public Ressources addSv(Fonction fonction) {
        this.svs.add(fonction);
        fonction.setRessources(this);
        return this;
    }

    public Ressources removeSv(Fonction fonction) {
        this.svs.remove(fonction);
        fonction.setRessources(null);
        return this;
    }

    public void setSvs(Set<Fonction> fonctions) {
        this.svs = fonctions;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Ressources)) {
            return false;
        }
        return id != null && id.equals(((Ressources) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Ressources{" +
            "id=" + getId() +
            ", nom='" + getNom() + "'" +
            ", prenom='" + getPrenom() + "'" +
            ", acronyme='" + getAcronyme() + "'" +
            "}";
    }
}
