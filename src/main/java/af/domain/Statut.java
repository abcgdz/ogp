package af.domain;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;

import java.io.Serializable;

/**
 * A Statut.
 */
@Entity
@Table(name = "statut")
public class Statut implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "libelle")
    private String libelle;

    @Column(name = "code")
    private Integer code;

    @ManyToOne
    @JsonIgnoreProperties("statuts")
    private Ressources ressources;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLibelle() {
        return libelle;
    }

    public Statut libelle(String libelle) {
        this.libelle = libelle;
        return this;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public Integer getCode() {
        return code;
    }

    public Statut code(Integer code) {
        this.code = code;
        return this;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public Ressources getRessources() {
        return ressources;
    }

    public Statut ressources(Ressources ressources) {
        this.ressources = ressources;
        return this;
    }

    public void setRessources(Ressources ressources) {
        this.ressources = ressources;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Statut)) {
            return false;
        }
        return id != null && id.equals(((Statut) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Statut{" +
            "id=" + getId() +
            ", libelle='" + getLibelle() + "'" +
            ", code=" + getCode() +
            "}";
    }
}
