package af.domain;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;

import java.io.Serializable;

/**
 * A Projet.
 */
@Entity
@Table(name = "projet")
public class Projet implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "acronyme")
    private String acronyme;

    @Column(name = "probabilite_gain")
    private Integer probabiliteGain;

    @Column(name = "charge_jh")
    private Integer chargeJh;

    @Column(name = "mois_de_demarrage")
    private String moisDeDemarrage;

    @Column(name = "duree")
    private String duree;

    @Column(name = "mois_de_cloture")
    private String moisDeCloture;

    @OneToOne
    @JoinColumn(unique = true)
    private Client a;

    @OneToOne
    @JoinColumn(unique = true)
    private TypeDeProjet typ;

    @ManyToOne
    @JsonIgnoreProperties("projets")
    private DetailPC detailPC;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAcronyme() {
        return acronyme;
    }

    public Projet acronyme(String acronyme) {
        this.acronyme = acronyme;
        return this;
    }

    public void setAcronyme(String acronyme) {
        this.acronyme = acronyme;
    }

    public Integer getProbabiliteGain() {
        return probabiliteGain;
    }

    public Projet probabiliteGain(Integer probabiliteGain) {
        this.probabiliteGain = probabiliteGain;
        return this;
    }

    public void setProbabiliteGain(Integer probabiliteGain) {
        this.probabiliteGain = probabiliteGain;
    }

    public Integer getChargeJh() {
        return chargeJh;
    }

    public Projet chargeJh(Integer chargeJh) {
        this.chargeJh = chargeJh;
        return this;
    }

    public void setChargeJh(Integer chargeJh) {
        this.chargeJh = chargeJh;
    }

    public String getMoisDeDemarrage() {
        return moisDeDemarrage;
    }

    public Projet moisDeDemarrage(String moisDeDemarrage) {
        this.moisDeDemarrage = moisDeDemarrage;
        return this;
    }

    public void setMoisDeDemarrage(String moisDeDemarrage) {
        this.moisDeDemarrage = moisDeDemarrage;
    }

    public String getDuree() {
        return duree;
    }

    public Projet duree(String duree) {
        this.duree = duree;
        return this;
    }

    public void setDuree(String duree) {
        this.duree = duree;
    }

    public String getMoisDeCloture() {
        return moisDeCloture;
    }

    public Projet moisDeCloture(String moisDeCloture) {
        this.moisDeCloture = moisDeCloture;
        return this;
    }

    public void setMoisDeCloture(String moisDeCloture) {
        this.moisDeCloture = moisDeCloture;
    }

    public Client getA() {
        return a;
    }

    public Projet a(Client client) {
        this.a = client;
        return this;
    }

    public void setA(Client client) {
        this.a = client;
    }

    public TypeDeProjet getTyp() {
        return typ;
    }

    public Projet typ(TypeDeProjet typeDeProjet) {
        this.typ = typeDeProjet;
        return this;
    }

    public void setTyp(TypeDeProjet typeDeProjet) {
        this.typ = typeDeProjet;
    }

    public DetailPC getDetailPC() {
        return detailPC;
    }

    public Projet detailPC(DetailPC detailPC) {
        this.detailPC = detailPC;
        return this;
    }

    public void setDetailPC(DetailPC detailPC) {
        this.detailPC = detailPC;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Projet)) {
            return false;
        }
        return id != null && id.equals(((Projet) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Projet{" +
            "id=" + getId() +
            ", acronyme='" + getAcronyme() + "'" +
            ", probabiliteGain=" + getProbabiliteGain() +
            ", chargeJh=" + getChargeJh() +
            ", moisDeDemarrage='" + getMoisDeDemarrage() + "'" +
            ", duree='" + getDuree() + "'" +
            ", moisDeCloture='" + getMoisDeCloture() + "'" +
            "}";
    }
}
