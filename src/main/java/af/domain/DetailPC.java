package af.domain;

import javax.persistence.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * A DetailPC.
 */
@Entity
@Table(name = "detail_pc")
public class DetailPC implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "libelle")
    private String libelle;

    @Column(name = "code")
    private Integer code;

    @Column(name = "role")
    private String role;

    @OneToMany(mappedBy = "detailPC")
    private Set<Projet> ts = new HashSet<>();

    @OneToMany(mappedBy = "detailPC")
    private Set<BU> tis = new HashSet<>();

    @OneToMany(mappedBy = "detailPC")
    private Set<Pc> kjks = new HashSet<>();

    @OneToMany(mappedBy = "detailPC")
    private Set<Duree> fvgs = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLibelle() {
        return libelle;
    }

    public DetailPC libelle(String libelle) {
        this.libelle = libelle;
        return this;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public Integer getCode() {
        return code;
    }

    public DetailPC code(Integer code) {
        this.code = code;
        return this;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getRole() {
        return role;
    }

    public DetailPC role(String role) {
        this.role = role;
        return this;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public Set<Projet> getTs() {
        return ts;
    }

    public DetailPC ts(Set<Projet> projets) {
        this.ts = projets;
        return this;
    }

    public DetailPC addT(Projet projet) {
        this.ts.add(projet);
        projet.setDetailPC(this);
        return this;
    }

    public DetailPC removeT(Projet projet) {
        this.ts.remove(projet);
        projet.setDetailPC(null);
        return this;
    }

    public void setTs(Set<Projet> projets) {
        this.ts = projets;
    }

    public Set<BU> getTis() {
        return tis;
    }

    public DetailPC tis(Set<BU> bUS) {
        this.tis = bUS;
        return this;
    }

    public DetailPC addTi(BU bU) {
        this.tis.add(bU);
        bU.setDetailPC(this);
        return this;
    }

    public DetailPC removeTi(BU bU) {
        this.tis.remove(bU);
        bU.setDetailPC(null);
        return this;
    }

    public void setTis(Set<BU> bUS) {
        this.tis = bUS;
    }

    public Set<Pc> getKjks() {
        return kjks;
    }

    public DetailPC kjks(Set<Pc> pcs) {
        this.kjks = pcs;
        return this;
    }

    public DetailPC addKjk(Pc pc) {
        this.kjks.add(pc);
        pc.setDetailPC(this);
        return this;
    }

    public DetailPC removeKjk(Pc pc) {
        this.kjks.remove(pc);
        pc.setDetailPC(null);
        return this;
    }

    public void setKjks(Set<Pc> pcs) {
        this.kjks = pcs;
    }

    public Set<Duree> getFvgs() {
        return fvgs;
    }

    public DetailPC fvgs(Set<Duree> durees) {
        this.fvgs = durees;
        return this;
    }

    public DetailPC addFvg(Duree duree) {
        this.fvgs.add(duree);
        duree.setDetailPC(this);
        return this;
    }

    public DetailPC removeFvg(Duree duree) {
        this.fvgs.remove(duree);
        duree.setDetailPC(null);
        return this;
    }

    public void setFvgs(Set<Duree> durees) {
        this.fvgs = durees;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof DetailPC)) {
            return false;
        }
        return id != null && id.equals(((DetailPC) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "DetailPC{" +
            "id=" + getId() +
            ", libelle='" + getLibelle() + "'" +
            ", code=" + getCode() +
            ", role='" + getRole() + "'" +
            "}";
    }
}
