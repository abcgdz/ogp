package af.domain;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;

import java.io.Serializable;

/**
 * A Duree.
 */
@Entity
@Table(name = "duree")
public class Duree implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "jour")
    private Integer jour;

    @Column(name = "mois")
    private String mois;

    @ManyToOne
    @JsonIgnoreProperties("durees")
    private DetailPC detailPC;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getJour() {
        return jour;
    }

    public Duree jour(Integer jour) {
        this.jour = jour;
        return this;
    }

    public void setJour(Integer jour) {
        this.jour = jour;
    }

    public String getMois() {
        return mois;
    }

    public Duree mois(String mois) {
        this.mois = mois;
        return this;
    }

    public void setMois(String mois) {
        this.mois = mois;
    }

    public DetailPC getDetailPC() {
        return detailPC;
    }

    public Duree detailPC(DetailPC detailPC) {
        this.detailPC = detailPC;
        return this;
    }

    public void setDetailPC(DetailPC detailPC) {
        this.detailPC = detailPC;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Duree)) {
            return false;
        }
        return id != null && id.equals(((Duree) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Duree{" +
            "id=" + getId() +
            ", jour=" + getJour() +
            ", mois='" + getMois() + "'" +
            "}";
    }
}
