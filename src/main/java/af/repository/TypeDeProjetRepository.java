package af.repository;

import af.domain.TypeDeProjet;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the TypeDeProjet entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TypeDeProjetRepository extends JpaRepository<TypeDeProjet, Long> {

}
