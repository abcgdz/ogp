package af.repository;

import af.domain.BU;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the BU entity.
 */
@SuppressWarnings("unused")
@Repository
public interface BURepository extends JpaRepository<BU, Long> {

}
