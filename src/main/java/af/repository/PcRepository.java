package af.repository;

import af.domain.Pc;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the Pc entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PcRepository extends JpaRepository<Pc, Long> {

}
