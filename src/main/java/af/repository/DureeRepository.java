package af.repository;

import af.domain.Duree;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the Duree entity.
 */
@SuppressWarnings("unused")
@Repository
public interface DureeRepository extends JpaRepository<Duree, Long> {

}
