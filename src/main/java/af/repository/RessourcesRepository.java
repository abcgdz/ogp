package af.repository;

import af.domain.Ressources;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the Ressources entity.
 */
@SuppressWarnings("unused")
@Repository
public interface RessourcesRepository extends JpaRepository<Ressources, Long> {

}
