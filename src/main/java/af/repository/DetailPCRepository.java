package af.repository;

import af.domain.DetailPC;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the DetailPC entity.
 */
@SuppressWarnings("unused")
@Repository
public interface DetailPCRepository extends JpaRepository<DetailPC, Long> {

}
