import { Component, OnInit } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiAlertService } from 'ng-jhipster';
import { IProjet, Projet } from 'app/shared/model/projet.model';
import { ProjetService } from './projet.service';
import { IClient } from 'app/shared/model/client.model';
import { ClientService } from 'app/entities/client';
import { ITypeDeProjet } from 'app/shared/model/type-de-projet.model';
import { TypeDeProjetService } from 'app/entities/type-de-projet';
import { IDetailPC } from 'app/shared/model/detail-pc.model';
import { DetailPCService } from 'app/entities/detail-pc';

@Component({
  selector: 'jhi-projet-update',
  templateUrl: './projet-update.component.html'
})
export class ProjetUpdateComponent implements OnInit {
  isSaving: boolean;

  as: IClient[];

  typs: ITypeDeProjet[];

  detailpcs: IDetailPC[];

  editForm = this.fb.group({
    id: [],
    acronyme: [],
    probabiliteGain: [],
    chargeJh: [],
    moisDeDemarrage: [],
    duree: [],
    moisDeCloture: [],
    a: [],
    typ: [],
    detailPC: []
  });

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected projetService: ProjetService,
    protected clientService: ClientService,
    protected typeDeProjetService: TypeDeProjetService,
    protected detailPCService: DetailPCService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ projet }) => {
      this.updateForm(projet);
    });
    this.clientService
      .query({ filter: 'projet-is-null' })
      .pipe(
        filter((mayBeOk: HttpResponse<IClient[]>) => mayBeOk.ok),
        map((response: HttpResponse<IClient[]>) => response.body)
      )
      .subscribe(
        (res: IClient[]) => {
          if (!this.editForm.get('a').value || !this.editForm.get('a').value.id) {
            this.as = res;
          } else {
            this.clientService
              .find(this.editForm.get('a').value.id)
              .pipe(
                filter((subResMayBeOk: HttpResponse<IClient>) => subResMayBeOk.ok),
                map((subResponse: HttpResponse<IClient>) => subResponse.body)
              )
              .subscribe(
                (subRes: IClient) => (this.as = [subRes].concat(res)),
                (subRes: HttpErrorResponse) => this.onError(subRes.message)
              );
          }
        },
        (res: HttpErrorResponse) => this.onError(res.message)
      );
    this.typeDeProjetService
      .query({ filter: 'projet-is-null' })
      .pipe(
        filter((mayBeOk: HttpResponse<ITypeDeProjet[]>) => mayBeOk.ok),
        map((response: HttpResponse<ITypeDeProjet[]>) => response.body)
      )
      .subscribe(
        (res: ITypeDeProjet[]) => {
          if (!this.editForm.get('typ').value || !this.editForm.get('typ').value.id) {
            this.typs = res;
          } else {
            this.typeDeProjetService
              .find(this.editForm.get('typ').value.id)
              .pipe(
                filter((subResMayBeOk: HttpResponse<ITypeDeProjet>) => subResMayBeOk.ok),
                map((subResponse: HttpResponse<ITypeDeProjet>) => subResponse.body)
              )
              .subscribe(
                (subRes: ITypeDeProjet) => (this.typs = [subRes].concat(res)),
                (subRes: HttpErrorResponse) => this.onError(subRes.message)
              );
          }
        },
        (res: HttpErrorResponse) => this.onError(res.message)
      );
    this.detailPCService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IDetailPC[]>) => mayBeOk.ok),
        map((response: HttpResponse<IDetailPC[]>) => response.body)
      )
      .subscribe((res: IDetailPC[]) => (this.detailpcs = res), (res: HttpErrorResponse) => this.onError(res.message));
  }

  updateForm(projet: IProjet) {
    this.editForm.patchValue({
      id: projet.id,
      acronyme: projet.acronyme,
      probabiliteGain: projet.probabiliteGain,
      chargeJh: projet.chargeJh,
      moisDeDemarrage: projet.moisDeDemarrage,
      duree: projet.duree,
      moisDeCloture: projet.moisDeCloture,
      a: projet.a,
      typ: projet.typ,
      detailPC: projet.detailPC
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const projet = this.createFromForm();
    if (projet.id !== undefined) {
      this.subscribeToSaveResponse(this.projetService.update(projet));
    } else {
      this.subscribeToSaveResponse(this.projetService.create(projet));
    }
  }

  private createFromForm(): IProjet {
    return {
      ...new Projet(),
      id: this.editForm.get(['id']).value,
      acronyme: this.editForm.get(['acronyme']).value,
      probabiliteGain: this.editForm.get(['probabiliteGain']).value,
      chargeJh: this.editForm.get(['chargeJh']).value,
      moisDeDemarrage: this.editForm.get(['moisDeDemarrage']).value,
      duree: this.editForm.get(['duree']).value,
      moisDeCloture: this.editForm.get(['moisDeCloture']).value,
      a: this.editForm.get(['a']).value,
      typ: this.editForm.get(['typ']).value,
      detailPC: this.editForm.get(['detailPC']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IProjet>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackClientById(index: number, item: IClient) {
    return item.id;
  }

  trackTypeDeProjetById(index: number, item: ITypeDeProjet) {
    return item.id;
  }

  trackDetailPCById(index: number, item: IDetailPC) {
    return item.id;
  }
}
