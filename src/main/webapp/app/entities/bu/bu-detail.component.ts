import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IBU } from 'app/shared/model/bu.model';

@Component({
  selector: 'jhi-bu-detail',
  templateUrl: './bu-detail.component.html'
})
export class BUDetailComponent implements OnInit {
  bU: IBU;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ bU }) => {
      this.bU = bU;
    });
  }

  previousState() {
    window.history.back();
  }
}
