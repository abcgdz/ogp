import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { IBU } from 'app/shared/model/bu.model';
import { AccountService } from 'app/core';
import { BUService } from './bu.service';

@Component({
  selector: 'jhi-bu',
  templateUrl: './bu.component.html'
})
export class BUComponent implements OnInit, OnDestroy {
  bUS: IBU[];
  currentAccount: any;
  eventSubscriber: Subscription;

  constructor(
    protected bUService: BUService,
    protected jhiAlertService: JhiAlertService,
    protected eventManager: JhiEventManager,
    protected accountService: AccountService
  ) {}

  loadAll() {
    this.bUService
      .query()
      .pipe(
        filter((res: HttpResponse<IBU[]>) => res.ok),
        map((res: HttpResponse<IBU[]>) => res.body)
      )
      .subscribe(
        (res: IBU[]) => {
          this.bUS = res;
        },
        (res: HttpErrorResponse) => this.onError(res.message)
      );
  }

  ngOnInit() {
    this.loadAll();
    this.accountService.identity().then(account => {
      this.currentAccount = account;
    });
    this.registerChangeInBUS();
  }

  ngOnDestroy() {
    this.eventManager.destroy(this.eventSubscriber);
  }

  trackId(index: number, item: IBU) {
    return item.id;
  }

  registerChangeInBUS() {
    this.eventSubscriber = this.eventManager.subscribe('bUListModification', response => this.loadAll());
  }

  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }
}
