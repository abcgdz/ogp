import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IBU } from 'app/shared/model/bu.model';
import { BUService } from './bu.service';

@Component({
  selector: 'jhi-bu-delete-dialog',
  templateUrl: './bu-delete-dialog.component.html'
})
export class BUDeleteDialogComponent {
  bU: IBU;

  constructor(protected bUService: BUService, public activeModal: NgbActiveModal, protected eventManager: JhiEventManager) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.bUService.delete(id).subscribe(response => {
      this.eventManager.broadcast({
        name: 'bUListModification',
        content: 'Deleted an bU'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-bu-delete-popup',
  template: ''
})
export class BUDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ bU }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(BUDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
        this.ngbModalRef.componentInstance.bU = bU;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/bu', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/bu', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
