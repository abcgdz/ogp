import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IBU } from 'app/shared/model/bu.model';

type EntityResponseType = HttpResponse<IBU>;
type EntityArrayResponseType = HttpResponse<IBU[]>;

@Injectable({ providedIn: 'root' })
export class BUService {
  public resourceUrl = SERVER_API_URL + 'api/bus';

  constructor(protected http: HttpClient) {}

  create(bU: IBU): Observable<EntityResponseType> {
    return this.http.post<IBU>(this.resourceUrl, bU, { observe: 'response' });
  }

  update(bU: IBU): Observable<EntityResponseType> {
    return this.http.put<IBU>(this.resourceUrl, bU, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IBU>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IBU[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
