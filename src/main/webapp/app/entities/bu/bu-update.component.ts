import { Component, OnInit } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiAlertService } from 'ng-jhipster';
import { IBU, BU } from 'app/shared/model/bu.model';
import { BUService } from './bu.service';
import { IProjet } from 'app/shared/model/projet.model';
import { ProjetService } from 'app/entities/projet';
import { IDetailPC } from 'app/shared/model/detail-pc.model';
import { DetailPCService } from 'app/entities/detail-pc';

@Component({
  selector: 'jhi-bu-update',
  templateUrl: './bu-update.component.html'
})
export class BUUpdateComponent implements OnInit {
  isSaving: boolean;

  ls: IProjet[];

  detailpcs: IDetailPC[];

  editForm = this.fb.group({
    id: [],
    nom: [],
    prenom: [],
    l: [],
    detailPC: []
  });

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected bUService: BUService,
    protected projetService: ProjetService,
    protected detailPCService: DetailPCService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ bU }) => {
      this.updateForm(bU);
    });
    this.projetService
      .query({ filter: 'bu-is-null' })
      .pipe(
        filter((mayBeOk: HttpResponse<IProjet[]>) => mayBeOk.ok),
        map((response: HttpResponse<IProjet[]>) => response.body)
      )
      .subscribe(
        (res: IProjet[]) => {
          if (!this.editForm.get('l').value || !this.editForm.get('l').value.id) {
            this.ls = res;
          } else {
            this.projetService
              .find(this.editForm.get('l').value.id)
              .pipe(
                filter((subResMayBeOk: HttpResponse<IProjet>) => subResMayBeOk.ok),
                map((subResponse: HttpResponse<IProjet>) => subResponse.body)
              )
              .subscribe(
                (subRes: IProjet) => (this.ls = [subRes].concat(res)),
                (subRes: HttpErrorResponse) => this.onError(subRes.message)
              );
          }
        },
        (res: HttpErrorResponse) => this.onError(res.message)
      );
    this.detailPCService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IDetailPC[]>) => mayBeOk.ok),
        map((response: HttpResponse<IDetailPC[]>) => response.body)
      )
      .subscribe((res: IDetailPC[]) => (this.detailpcs = res), (res: HttpErrorResponse) => this.onError(res.message));
  }

  updateForm(bU: IBU) {
    this.editForm.patchValue({
      id: bU.id,
      nom: bU.nom,
      prenom: bU.prenom,
      l: bU.l,
      detailPC: bU.detailPC
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const bU = this.createFromForm();
    if (bU.id !== undefined) {
      this.subscribeToSaveResponse(this.bUService.update(bU));
    } else {
      this.subscribeToSaveResponse(this.bUService.create(bU));
    }
  }

  private createFromForm(): IBU {
    return {
      ...new BU(),
      id: this.editForm.get(['id']).value,
      nom: this.editForm.get(['nom']).value,
      prenom: this.editForm.get(['prenom']).value,
      l: this.editForm.get(['l']).value,
      detailPC: this.editForm.get(['detailPC']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IBU>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackProjetById(index: number, item: IProjet) {
    return item.id;
  }

  trackDetailPCById(index: number, item: IDetailPC) {
    return item.id;
  }
}
