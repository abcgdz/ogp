import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { BU } from 'app/shared/model/bu.model';
import { BUService } from './bu.service';
import { BUComponent } from './bu.component';
import { BUDetailComponent } from './bu-detail.component';
import { BUUpdateComponent } from './bu-update.component';
import { BUDeletePopupComponent } from './bu-delete-dialog.component';
import { IBU } from 'app/shared/model/bu.model';

@Injectable({ providedIn: 'root' })
export class BUResolve implements Resolve<IBU> {
  constructor(private service: BUService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IBU> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<BU>) => response.ok),
        map((bU: HttpResponse<BU>) => bU.body)
      );
    }
    return of(new BU());
  }
}

export const bURoute: Routes = [
  {
    path: '',
    component: BUComponent,
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'BUS'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: BUDetailComponent,
    resolve: {
      bU: BUResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'BUS'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: BUUpdateComponent,
    resolve: {
      bU: BUResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'BUS'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: BUUpdateComponent,
    resolve: {
      bU: BUResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'BUS'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const bUPopupRoute: Routes = [
  {
    path: ':id/delete',
    component: BUDeletePopupComponent,
    resolve: {
      bU: BUResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'BUS'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
