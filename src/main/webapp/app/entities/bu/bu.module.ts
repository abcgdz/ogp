import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { OgpSharedModule } from 'app/shared';
import {
  BUComponent,
  BUDetailComponent,
  BUUpdateComponent,
  BUDeletePopupComponent,
  BUDeleteDialogComponent,
  bURoute,
  bUPopupRoute
} from './';

const ENTITY_STATES = [...bURoute, ...bUPopupRoute];

@NgModule({
  imports: [OgpSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [BUComponent, BUDetailComponent, BUUpdateComponent, BUDeleteDialogComponent, BUDeletePopupComponent],
  entryComponents: [BUComponent, BUUpdateComponent, BUDeleteDialogComponent, BUDeletePopupComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class OgpBUModule {}
