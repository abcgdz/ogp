import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IRessources } from 'app/shared/model/ressources.model';
import { RessourcesService } from './ressources.service';

@Component({
  selector: 'jhi-ressources-delete-dialog',
  templateUrl: './ressources-delete-dialog.component.html'
})
export class RessourcesDeleteDialogComponent {
  ressources: IRessources;

  constructor(
    protected ressourcesService: RessourcesService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.ressourcesService.delete(id).subscribe(response => {
      this.eventManager.broadcast({
        name: 'ressourcesListModification',
        content: 'Deleted an ressources'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-ressources-delete-popup',
  template: ''
})
export class RessourcesDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ ressources }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(RessourcesDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
        this.ngbModalRef.componentInstance.ressources = ressources;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/ressources', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/ressources', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
