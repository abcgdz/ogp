import { Component, OnInit } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiAlertService } from 'ng-jhipster';
import { IRessources, Ressources } from 'app/shared/model/ressources.model';
import { RessourcesService } from './ressources.service';
import { IBU } from 'app/shared/model/bu.model';
import { BUService } from 'app/entities/bu';

@Component({
  selector: 'jhi-ressources-update',
  templateUrl: './ressources-update.component.html'
})
export class RessourcesUpdateComponent implements OnInit {
  isSaving: boolean;

  bus: IBU[];

  editForm = this.fb.group({
    id: [],
    nom: [],
    prenom: [],
    acronyme: [],
    bU: []
  });

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected ressourcesService: RessourcesService,
    protected bUService: BUService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ ressources }) => {
      this.updateForm(ressources);
    });
    this.bUService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IBU[]>) => mayBeOk.ok),
        map((response: HttpResponse<IBU[]>) => response.body)
      )
      .subscribe((res: IBU[]) => (this.bus = res), (res: HttpErrorResponse) => this.onError(res.message));
  }

  updateForm(ressources: IRessources) {
    this.editForm.patchValue({
      id: ressources.id,
      nom: ressources.nom,
      prenom: ressources.prenom,
      acronyme: ressources.acronyme,
      bU: ressources.bU
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const ressources = this.createFromForm();
    if (ressources.id !== undefined) {
      this.subscribeToSaveResponse(this.ressourcesService.update(ressources));
    } else {
      this.subscribeToSaveResponse(this.ressourcesService.create(ressources));
    }
  }

  private createFromForm(): IRessources {
    return {
      ...new Ressources(),
      id: this.editForm.get(['id']).value,
      nom: this.editForm.get(['nom']).value,
      prenom: this.editForm.get(['prenom']).value,
      acronyme: this.editForm.get(['acronyme']).value,
      bU: this.editForm.get(['bU']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IRessources>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackBUById(index: number, item: IBU) {
    return item.id;
  }
}
