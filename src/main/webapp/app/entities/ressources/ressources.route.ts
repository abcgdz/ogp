import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { Ressources } from 'app/shared/model/ressources.model';
import { RessourcesService } from './ressources.service';
import { RessourcesComponent } from './ressources.component';
import { RessourcesDetailComponent } from './ressources-detail.component';
import { RessourcesUpdateComponent } from './ressources-update.component';
import { RessourcesDeletePopupComponent } from './ressources-delete-dialog.component';
import { IRessources } from 'app/shared/model/ressources.model';

@Injectable({ providedIn: 'root' })
export class RessourcesResolve implements Resolve<IRessources> {
  constructor(private service: RessourcesService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IRessources> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<Ressources>) => response.ok),
        map((ressources: HttpResponse<Ressources>) => ressources.body)
      );
    }
    return of(new Ressources());
  }
}

export const ressourcesRoute: Routes = [
  {
    path: '',
    component: RessourcesComponent,
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'Ressources'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: RessourcesDetailComponent,
    resolve: {
      ressources: RessourcesResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'Ressources'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: RessourcesUpdateComponent,
    resolve: {
      ressources: RessourcesResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'Ressources'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: RessourcesUpdateComponent,
    resolve: {
      ressources: RessourcesResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'Ressources'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const ressourcesPopupRoute: Routes = [
  {
    path: ':id/delete',
    component: RessourcesDeletePopupComponent,
    resolve: {
      ressources: RessourcesResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'Ressources'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
