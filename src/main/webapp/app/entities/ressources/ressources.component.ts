import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { IRessources } from 'app/shared/model/ressources.model';
import { AccountService } from 'app/core';
import { RessourcesService } from './ressources.service';

@Component({
  selector: 'jhi-ressources',
  templateUrl: './ressources.component.html'
})
export class RessourcesComponent implements OnInit, OnDestroy {
  ressources: IRessources[];
  currentAccount: any;
  eventSubscriber: Subscription;

  constructor(
    protected ressourcesService: RessourcesService,
    protected jhiAlertService: JhiAlertService,
    protected eventManager: JhiEventManager,
    protected accountService: AccountService
  ) {}

  loadAll() {
    this.ressourcesService
      .query()
      .pipe(
        filter((res: HttpResponse<IRessources[]>) => res.ok),
        map((res: HttpResponse<IRessources[]>) => res.body)
      )
      .subscribe(
        (res: IRessources[]) => {
          this.ressources = res;
        },
        (res: HttpErrorResponse) => this.onError(res.message)
      );
  }

  ngOnInit() {
    this.loadAll();
    this.accountService.identity().then(account => {
      this.currentAccount = account;
    });
    this.registerChangeInRessources();
  }

  ngOnDestroy() {
    this.eventManager.destroy(this.eventSubscriber);
  }

  trackId(index: number, item: IRessources) {
    return item.id;
  }

  registerChangeInRessources() {
    this.eventSubscriber = this.eventManager.subscribe('ressourcesListModification', response => this.loadAll());
  }

  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }
}
