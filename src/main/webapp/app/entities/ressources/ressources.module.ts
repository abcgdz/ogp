import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { OgpSharedModule } from 'app/shared';
import {
  RessourcesComponent,
  RessourcesDetailComponent,
  RessourcesUpdateComponent,
  RessourcesDeletePopupComponent,
  RessourcesDeleteDialogComponent,
  ressourcesRoute,
  ressourcesPopupRoute
} from './';

const ENTITY_STATES = [...ressourcesRoute, ...ressourcesPopupRoute];

@NgModule({
  imports: [OgpSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    RessourcesComponent,
    RessourcesDetailComponent,
    RessourcesUpdateComponent,
    RessourcesDeleteDialogComponent,
    RessourcesDeletePopupComponent
  ],
  entryComponents: [RessourcesComponent, RessourcesUpdateComponent, RessourcesDeleteDialogComponent, RessourcesDeletePopupComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class OgpRessourcesModule {}
