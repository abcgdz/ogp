export * from './ressources.service';
export * from './ressources-update.component';
export * from './ressources-delete-dialog.component';
export * from './ressources-detail.component';
export * from './ressources.component';
export * from './ressources.route';
