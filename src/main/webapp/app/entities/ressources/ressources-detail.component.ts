import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IRessources } from 'app/shared/model/ressources.model';

@Component({
  selector: 'jhi-ressources-detail',
  templateUrl: './ressources-detail.component.html'
})
export class RessourcesDetailComponent implements OnInit {
  ressources: IRessources;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ ressources }) => {
      this.ressources = ressources;
    });
  }

  previousState() {
    window.history.back();
  }
}
