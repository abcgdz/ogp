import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { Duree } from 'app/shared/model/duree.model';
import { DureeService } from './duree.service';
import { DureeComponent } from './duree.component';
import { DureeDetailComponent } from './duree-detail.component';
import { DureeUpdateComponent } from './duree-update.component';
import { DureeDeletePopupComponent } from './duree-delete-dialog.component';
import { IDuree } from 'app/shared/model/duree.model';

@Injectable({ providedIn: 'root' })
export class DureeResolve implements Resolve<IDuree> {
  constructor(private service: DureeService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IDuree> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<Duree>) => response.ok),
        map((duree: HttpResponse<Duree>) => duree.body)
      );
    }
    return of(new Duree());
  }
}

export const dureeRoute: Routes = [
  {
    path: '',
    component: DureeComponent,
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'Durees'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: DureeDetailComponent,
    resolve: {
      duree: DureeResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'Durees'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: DureeUpdateComponent,
    resolve: {
      duree: DureeResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'Durees'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: DureeUpdateComponent,
    resolve: {
      duree: DureeResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'Durees'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const dureePopupRoute: Routes = [
  {
    path: ':id/delete',
    component: DureeDeletePopupComponent,
    resolve: {
      duree: DureeResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'Durees'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
