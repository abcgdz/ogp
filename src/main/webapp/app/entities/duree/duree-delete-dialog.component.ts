import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IDuree } from 'app/shared/model/duree.model';
import { DureeService } from './duree.service';

@Component({
  selector: 'jhi-duree-delete-dialog',
  templateUrl: './duree-delete-dialog.component.html'
})
export class DureeDeleteDialogComponent {
  duree: IDuree;

  constructor(protected dureeService: DureeService, public activeModal: NgbActiveModal, protected eventManager: JhiEventManager) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.dureeService.delete(id).subscribe(response => {
      this.eventManager.broadcast({
        name: 'dureeListModification',
        content: 'Deleted an duree'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-duree-delete-popup',
  template: ''
})
export class DureeDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ duree }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(DureeDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
        this.ngbModalRef.componentInstance.duree = duree;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/duree', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/duree', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
