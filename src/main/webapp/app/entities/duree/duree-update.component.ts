import { Component, OnInit } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiAlertService } from 'ng-jhipster';
import { IDuree, Duree } from 'app/shared/model/duree.model';
import { DureeService } from './duree.service';
import { IDetailPC } from 'app/shared/model/detail-pc.model';
import { DetailPCService } from 'app/entities/detail-pc';

@Component({
  selector: 'jhi-duree-update',
  templateUrl: './duree-update.component.html'
})
export class DureeUpdateComponent implements OnInit {
  isSaving: boolean;

  detailpcs: IDetailPC[];

  editForm = this.fb.group({
    id: [],
    jour: [],
    mois: [],
    detailPC: []
  });

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected dureeService: DureeService,
    protected detailPCService: DetailPCService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ duree }) => {
      this.updateForm(duree);
    });
    this.detailPCService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IDetailPC[]>) => mayBeOk.ok),
        map((response: HttpResponse<IDetailPC[]>) => response.body)
      )
      .subscribe((res: IDetailPC[]) => (this.detailpcs = res), (res: HttpErrorResponse) => this.onError(res.message));
  }

  updateForm(duree: IDuree) {
    this.editForm.patchValue({
      id: duree.id,
      jour: duree.jour,
      mois: duree.mois,
      detailPC: duree.detailPC
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const duree = this.createFromForm();
    if (duree.id !== undefined) {
      this.subscribeToSaveResponse(this.dureeService.update(duree));
    } else {
      this.subscribeToSaveResponse(this.dureeService.create(duree));
    }
  }

  private createFromForm(): IDuree {
    return {
      ...new Duree(),
      id: this.editForm.get(['id']).value,
      jour: this.editForm.get(['jour']).value,
      mois: this.editForm.get(['mois']).value,
      detailPC: this.editForm.get(['detailPC']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IDuree>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackDetailPCById(index: number, item: IDetailPC) {
    return item.id;
  }
}
