import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { IDuree } from 'app/shared/model/duree.model';
import { AccountService } from 'app/core';
import { DureeService } from './duree.service';

@Component({
  selector: 'jhi-duree',
  templateUrl: './duree.component.html'
})
export class DureeComponent implements OnInit, OnDestroy {
  durees: IDuree[];
  currentAccount: any;
  eventSubscriber: Subscription;

  constructor(
    protected dureeService: DureeService,
    protected jhiAlertService: JhiAlertService,
    protected eventManager: JhiEventManager,
    protected accountService: AccountService
  ) {}

  loadAll() {
    this.dureeService
      .query()
      .pipe(
        filter((res: HttpResponse<IDuree[]>) => res.ok),
        map((res: HttpResponse<IDuree[]>) => res.body)
      )
      .subscribe(
        (res: IDuree[]) => {
          this.durees = res;
        },
        (res: HttpErrorResponse) => this.onError(res.message)
      );
  }

  ngOnInit() {
    this.loadAll();
    this.accountService.identity().then(account => {
      this.currentAccount = account;
    });
    this.registerChangeInDurees();
  }

  ngOnDestroy() {
    this.eventManager.destroy(this.eventSubscriber);
  }

  trackId(index: number, item: IDuree) {
    return item.id;
  }

  registerChangeInDurees() {
    this.eventSubscriber = this.eventManager.subscribe('dureeListModification', response => this.loadAll());
  }

  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }
}
