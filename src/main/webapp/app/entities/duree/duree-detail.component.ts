import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IDuree } from 'app/shared/model/duree.model';

@Component({
  selector: 'jhi-duree-detail',
  templateUrl: './duree-detail.component.html'
})
export class DureeDetailComponent implements OnInit {
  duree: IDuree;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ duree }) => {
      this.duree = duree;
    });
  }

  previousState() {
    window.history.back();
  }
}
