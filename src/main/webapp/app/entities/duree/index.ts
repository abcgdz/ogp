export * from './duree.service';
export * from './duree-update.component';
export * from './duree-delete-dialog.component';
export * from './duree-detail.component';
export * from './duree.component';
export * from './duree.route';
