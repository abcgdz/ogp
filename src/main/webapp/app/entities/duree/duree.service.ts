import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IDuree } from 'app/shared/model/duree.model';

type EntityResponseType = HttpResponse<IDuree>;
type EntityArrayResponseType = HttpResponse<IDuree[]>;

@Injectable({ providedIn: 'root' })
export class DureeService {
  public resourceUrl = SERVER_API_URL + 'api/durees';

  constructor(protected http: HttpClient) {}

  create(duree: IDuree): Observable<EntityResponseType> {
    return this.http.post<IDuree>(this.resourceUrl, duree, { observe: 'response' });
  }

  update(duree: IDuree): Observable<EntityResponseType> {
    return this.http.put<IDuree>(this.resourceUrl, duree, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IDuree>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IDuree[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
