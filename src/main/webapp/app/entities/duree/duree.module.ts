import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { OgpSharedModule } from 'app/shared';
import {
  DureeComponent,
  DureeDetailComponent,
  DureeUpdateComponent,
  DureeDeletePopupComponent,
  DureeDeleteDialogComponent,
  dureeRoute,
  dureePopupRoute
} from './';

const ENTITY_STATES = [...dureeRoute, ...dureePopupRoute];

@NgModule({
  imports: [OgpSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [DureeComponent, DureeDetailComponent, DureeUpdateComponent, DureeDeleteDialogComponent, DureeDeletePopupComponent],
  entryComponents: [DureeComponent, DureeUpdateComponent, DureeDeleteDialogComponent, DureeDeletePopupComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class OgpDureeModule {}
