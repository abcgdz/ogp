import { Component, OnInit } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiAlertService } from 'ng-jhipster';
import { IStatut, Statut } from 'app/shared/model/statut.model';
import { StatutService } from './statut.service';
import { IRessources } from 'app/shared/model/ressources.model';
import { RessourcesService } from 'app/entities/ressources';

@Component({
  selector: 'jhi-statut-update',
  templateUrl: './statut-update.component.html'
})
export class StatutUpdateComponent implements OnInit {
  isSaving: boolean;

  ressources: IRessources[];

  editForm = this.fb.group({
    id: [],
    libelle: [],
    code: [],
    ressources: []
  });

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected statutService: StatutService,
    protected ressourcesService: RessourcesService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ statut }) => {
      this.updateForm(statut);
    });
    this.ressourcesService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IRessources[]>) => mayBeOk.ok),
        map((response: HttpResponse<IRessources[]>) => response.body)
      )
      .subscribe((res: IRessources[]) => (this.ressources = res), (res: HttpErrorResponse) => this.onError(res.message));
  }

  updateForm(statut: IStatut) {
    this.editForm.patchValue({
      id: statut.id,
      libelle: statut.libelle,
      code: statut.code,
      ressources: statut.ressources
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const statut = this.createFromForm();
    if (statut.id !== undefined) {
      this.subscribeToSaveResponse(this.statutService.update(statut));
    } else {
      this.subscribeToSaveResponse(this.statutService.create(statut));
    }
  }

  private createFromForm(): IStatut {
    return {
      ...new Statut(),
      id: this.editForm.get(['id']).value,
      libelle: this.editForm.get(['libelle']).value,
      code: this.editForm.get(['code']).value,
      ressources: this.editForm.get(['ressources']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IStatut>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackRessourcesById(index: number, item: IRessources) {
    return item.id;
  }
}
