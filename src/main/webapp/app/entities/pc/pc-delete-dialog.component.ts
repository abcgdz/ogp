import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IPc } from 'app/shared/model/pc.model';
import { PcService } from './pc.service';

@Component({
  selector: 'jhi-pc-delete-dialog',
  templateUrl: './pc-delete-dialog.component.html'
})
export class PcDeleteDialogComponent {
  pc: IPc;

  constructor(protected pcService: PcService, public activeModal: NgbActiveModal, protected eventManager: JhiEventManager) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.pcService.delete(id).subscribe(response => {
      this.eventManager.broadcast({
        name: 'pcListModification',
        content: 'Deleted an pc'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-pc-delete-popup',
  template: ''
})
export class PcDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ pc }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(PcDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
        this.ngbModalRef.componentInstance.pc = pc;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/pc', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/pc', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
