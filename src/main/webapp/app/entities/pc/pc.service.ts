import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IPc } from 'app/shared/model/pc.model';

type EntityResponseType = HttpResponse<IPc>;
type EntityArrayResponseType = HttpResponse<IPc[]>;

@Injectable({ providedIn: 'root' })
export class PcService {
  public resourceUrl = SERVER_API_URL + 'api/pcs';

  constructor(protected http: HttpClient) {}

  create(pc: IPc): Observable<EntityResponseType> {
    return this.http.post<IPc>(this.resourceUrl, pc, { observe: 'response' });
  }

  update(pc: IPc): Observable<EntityResponseType> {
    return this.http.put<IPc>(this.resourceUrl, pc, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IPc>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IPc[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
