import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { OgpSharedModule } from 'app/shared';
import {
  PcComponent,
  PcDetailComponent,
  PcUpdateComponent,
  PcDeletePopupComponent,
  PcDeleteDialogComponent,
  pcRoute,
  pcPopupRoute
} from './';

const ENTITY_STATES = [...pcRoute, ...pcPopupRoute];

@NgModule({
  imports: [OgpSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [PcComponent, PcDetailComponent, PcUpdateComponent, PcDeleteDialogComponent, PcDeletePopupComponent],
  entryComponents: [PcComponent, PcUpdateComponent, PcDeleteDialogComponent, PcDeletePopupComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class OgpPcModule {}
