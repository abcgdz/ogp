import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IPc } from 'app/shared/model/pc.model';

@Component({
  selector: 'jhi-pc-detail',
  templateUrl: './pc-detail.component.html'
})
export class PcDetailComponent implements OnInit {
  pc: IPc;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ pc }) => {
      this.pc = pc;
    });
  }

  previousState() {
    window.history.back();
  }
}
