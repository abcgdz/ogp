import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { Pc } from 'app/shared/model/pc.model';
import { PcService } from './pc.service';
import { PcComponent } from './pc.component';
import { PcDetailComponent } from './pc-detail.component';
import { PcUpdateComponent } from './pc-update.component';
import { PcDeletePopupComponent } from './pc-delete-dialog.component';
import { IPc } from 'app/shared/model/pc.model';

@Injectable({ providedIn: 'root' })
export class PcResolve implements Resolve<IPc> {
  constructor(private service: PcService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IPc> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<Pc>) => response.ok),
        map((pc: HttpResponse<Pc>) => pc.body)
      );
    }
    return of(new Pc());
  }
}

export const pcRoute: Routes = [
  {
    path: '',
    component: PcComponent,
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'Pcs'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: PcDetailComponent,
    resolve: {
      pc: PcResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'Pcs'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: PcUpdateComponent,
    resolve: {
      pc: PcResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'Pcs'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: PcUpdateComponent,
    resolve: {
      pc: PcResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'Pcs'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const pcPopupRoute: Routes = [
  {
    path: ':id/delete',
    component: PcDeletePopupComponent,
    resolve: {
      pc: PcResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'Pcs'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
