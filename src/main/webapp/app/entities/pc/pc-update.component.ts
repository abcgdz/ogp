import { Component, OnInit } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiAlertService } from 'ng-jhipster';
import { IPc, Pc } from 'app/shared/model/pc.model';
import { PcService } from './pc.service';
import { IDetailPC } from 'app/shared/model/detail-pc.model';
import { DetailPCService } from 'app/entities/detail-pc';

@Component({
  selector: 'jhi-pc-update',
  templateUrl: './pc-update.component.html'
})
export class PcUpdateComponent implements OnInit {
  isSaving: boolean;

  detailpcs: IDetailPC[];

  editForm = this.fb.group({
    id: [],
    libelle: [],
    code: [],
    detailPC: []
  });

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected pcService: PcService,
    protected detailPCService: DetailPCService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ pc }) => {
      this.updateForm(pc);
    });
    this.detailPCService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IDetailPC[]>) => mayBeOk.ok),
        map((response: HttpResponse<IDetailPC[]>) => response.body)
      )
      .subscribe((res: IDetailPC[]) => (this.detailpcs = res), (res: HttpErrorResponse) => this.onError(res.message));
  }

  updateForm(pc: IPc) {
    this.editForm.patchValue({
      id: pc.id,
      libelle: pc.libelle,
      code: pc.code,
      detailPC: pc.detailPC
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const pc = this.createFromForm();
    if (pc.id !== undefined) {
      this.subscribeToSaveResponse(this.pcService.update(pc));
    } else {
      this.subscribeToSaveResponse(this.pcService.create(pc));
    }
  }

  private createFromForm(): IPc {
    return {
      ...new Pc(),
      id: this.editForm.get(['id']).value,
      libelle: this.editForm.get(['libelle']).value,
      code: this.editForm.get(['code']).value,
      detailPC: this.editForm.get(['detailPC']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IPc>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackDetailPCById(index: number, item: IDetailPC) {
    return item.id;
  }
}
