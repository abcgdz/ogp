export * from './pc.service';
export * from './pc-update.component';
export * from './pc-delete-dialog.component';
export * from './pc-detail.component';
export * from './pc.component';
export * from './pc.route';
