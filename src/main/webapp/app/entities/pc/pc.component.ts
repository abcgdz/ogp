import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { IPc } from 'app/shared/model/pc.model';
import { AccountService } from 'app/core';
import { PcService } from './pc.service';

@Component({
  selector: 'jhi-pc',
  templateUrl: './pc.component.html'
})
export class PcComponent implements OnInit, OnDestroy {
  pcs: IPc[];
  currentAccount: any;
  eventSubscriber: Subscription;

  constructor(
    protected pcService: PcService,
    protected jhiAlertService: JhiAlertService,
    protected eventManager: JhiEventManager,
    protected accountService: AccountService
  ) {}

  loadAll() {
    this.pcService
      .query()
      .pipe(
        filter((res: HttpResponse<IPc[]>) => res.ok),
        map((res: HttpResponse<IPc[]>) => res.body)
      )
      .subscribe(
        (res: IPc[]) => {
          this.pcs = res;
        },
        (res: HttpErrorResponse) => this.onError(res.message)
      );
  }

  ngOnInit() {
    this.loadAll();
    this.accountService.identity().then(account => {
      this.currentAccount = account;
    });
    this.registerChangeInPcs();
  }

  ngOnDestroy() {
    this.eventManager.destroy(this.eventSubscriber);
  }

  trackId(index: number, item: IPc) {
    return item.id;
  }

  registerChangeInPcs() {
    this.eventSubscriber = this.eventManager.subscribe('pcListModification', response => this.loadAll());
  }

  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }
}
