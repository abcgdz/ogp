export * from './detail-pc.service';
export * from './detail-pc-update.component';
export * from './detail-pc-delete-dialog.component';
export * from './detail-pc-detail.component';
export * from './detail-pc.component';
export * from './detail-pc.route';
