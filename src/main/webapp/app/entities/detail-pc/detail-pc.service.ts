import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IDetailPC } from 'app/shared/model/detail-pc.model';

type EntityResponseType = HttpResponse<IDetailPC>;
type EntityArrayResponseType = HttpResponse<IDetailPC[]>;

@Injectable({ providedIn: 'root' })
export class DetailPCService {
  public resourceUrl = SERVER_API_URL + 'api/detail-pcs';

  constructor(protected http: HttpClient) {}

  create(detailPC: IDetailPC): Observable<EntityResponseType> {
    return this.http.post<IDetailPC>(this.resourceUrl, detailPC, { observe: 'response' });
  }

  update(detailPC: IDetailPC): Observable<EntityResponseType> {
    return this.http.put<IDetailPC>(this.resourceUrl, detailPC, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IDetailPC>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IDetailPC[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
