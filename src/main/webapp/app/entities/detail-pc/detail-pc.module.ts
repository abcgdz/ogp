import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { OgpSharedModule } from 'app/shared';
import {
  DetailPCComponent,
  DetailPCDetailComponent,
  DetailPCUpdateComponent,
  DetailPCDeletePopupComponent,
  DetailPCDeleteDialogComponent,
  detailPCRoute,
  detailPCPopupRoute
} from './';

const ENTITY_STATES = [...detailPCRoute, ...detailPCPopupRoute];

@NgModule({
  imports: [OgpSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    DetailPCComponent,
    DetailPCDetailComponent,
    DetailPCUpdateComponent,
    DetailPCDeleteDialogComponent,
    DetailPCDeletePopupComponent
  ],
  entryComponents: [DetailPCComponent, DetailPCUpdateComponent, DetailPCDeleteDialogComponent, DetailPCDeletePopupComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class OgpDetailPCModule {}
