import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IDetailPC } from 'app/shared/model/detail-pc.model';

@Component({
  selector: 'jhi-detail-pc-detail',
  templateUrl: './detail-pc-detail.component.html'
})
export class DetailPCDetailComponent implements OnInit {
  detailPC: IDetailPC;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ detailPC }) => {
      this.detailPC = detailPC;
    });
  }

  previousState() {
    window.history.back();
  }
}
