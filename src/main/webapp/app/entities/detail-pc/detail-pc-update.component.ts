import { Component, OnInit } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { IDetailPC, DetailPC } from 'app/shared/model/detail-pc.model';
import { DetailPCService } from './detail-pc.service';

@Component({
  selector: 'jhi-detail-pc-update',
  templateUrl: './detail-pc-update.component.html'
})
export class DetailPCUpdateComponent implements OnInit {
  isSaving: boolean;

  editForm = this.fb.group({
    id: [],
    libelle: [],
    code: [],
    role: []
  });

  constructor(protected detailPCService: DetailPCService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ detailPC }) => {
      this.updateForm(detailPC);
    });
  }

  updateForm(detailPC: IDetailPC) {
    this.editForm.patchValue({
      id: detailPC.id,
      libelle: detailPC.libelle,
      code: detailPC.code,
      role: detailPC.role
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const detailPC = this.createFromForm();
    if (detailPC.id !== undefined) {
      this.subscribeToSaveResponse(this.detailPCService.update(detailPC));
    } else {
      this.subscribeToSaveResponse(this.detailPCService.create(detailPC));
    }
  }

  private createFromForm(): IDetailPC {
    return {
      ...new DetailPC(),
      id: this.editForm.get(['id']).value,
      libelle: this.editForm.get(['libelle']).value,
      code: this.editForm.get(['code']).value,
      role: this.editForm.get(['role']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IDetailPC>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
}
