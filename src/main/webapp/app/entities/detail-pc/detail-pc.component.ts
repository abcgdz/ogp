import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { IDetailPC } from 'app/shared/model/detail-pc.model';
import { AccountService } from 'app/core';
import { DetailPCService } from './detail-pc.service';

@Component({
  selector: 'jhi-detail-pc',
  templateUrl: './detail-pc.component.html'
})
export class DetailPCComponent implements OnInit, OnDestroy {
  detailPCS: IDetailPC[];
  currentAccount: any;
  eventSubscriber: Subscription;

  constructor(
    protected detailPCService: DetailPCService,
    protected jhiAlertService: JhiAlertService,
    protected eventManager: JhiEventManager,
    protected accountService: AccountService
  ) {}

  loadAll() {
    this.detailPCService
      .query()
      .pipe(
        filter((res: HttpResponse<IDetailPC[]>) => res.ok),
        map((res: HttpResponse<IDetailPC[]>) => res.body)
      )
      .subscribe(
        (res: IDetailPC[]) => {
          this.detailPCS = res;
        },
        (res: HttpErrorResponse) => this.onError(res.message)
      );
  }

  ngOnInit() {
    this.loadAll();
    this.accountService.identity().then(account => {
      this.currentAccount = account;
    });
    this.registerChangeInDetailPCS();
  }

  ngOnDestroy() {
    this.eventManager.destroy(this.eventSubscriber);
  }

  trackId(index: number, item: IDetailPC) {
    return item.id;
  }

  registerChangeInDetailPCS() {
    this.eventSubscriber = this.eventManager.subscribe('detailPCListModification', response => this.loadAll());
  }

  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }
}
