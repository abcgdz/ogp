import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { DetailPC } from 'app/shared/model/detail-pc.model';
import { DetailPCService } from './detail-pc.service';
import { DetailPCComponent } from './detail-pc.component';
import { DetailPCDetailComponent } from './detail-pc-detail.component';
import { DetailPCUpdateComponent } from './detail-pc-update.component';
import { DetailPCDeletePopupComponent } from './detail-pc-delete-dialog.component';
import { IDetailPC } from 'app/shared/model/detail-pc.model';

@Injectable({ providedIn: 'root' })
export class DetailPCResolve implements Resolve<IDetailPC> {
  constructor(private service: DetailPCService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IDetailPC> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<DetailPC>) => response.ok),
        map((detailPC: HttpResponse<DetailPC>) => detailPC.body)
      );
    }
    return of(new DetailPC());
  }
}

export const detailPCRoute: Routes = [
  {
    path: '',
    component: DetailPCComponent,
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'DetailPCS'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: DetailPCDetailComponent,
    resolve: {
      detailPC: DetailPCResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'DetailPCS'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: DetailPCUpdateComponent,
    resolve: {
      detailPC: DetailPCResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'DetailPCS'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: DetailPCUpdateComponent,
    resolve: {
      detailPC: DetailPCResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'DetailPCS'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const detailPCPopupRoute: Routes = [
  {
    path: ':id/delete',
    component: DetailPCDeletePopupComponent,
    resolve: {
      detailPC: DetailPCResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'DetailPCS'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
