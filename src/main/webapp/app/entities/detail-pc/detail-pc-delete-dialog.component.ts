import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IDetailPC } from 'app/shared/model/detail-pc.model';
import { DetailPCService } from './detail-pc.service';

@Component({
  selector: 'jhi-detail-pc-delete-dialog',
  templateUrl: './detail-pc-delete-dialog.component.html'
})
export class DetailPCDeleteDialogComponent {
  detailPC: IDetailPC;

  constructor(protected detailPCService: DetailPCService, public activeModal: NgbActiveModal, protected eventManager: JhiEventManager) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.detailPCService.delete(id).subscribe(response => {
      this.eventManager.broadcast({
        name: 'detailPCListModification',
        content: 'Deleted an detailPC'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-detail-pc-delete-popup',
  template: ''
})
export class DetailPCDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ detailPC }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(DetailPCDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
        this.ngbModalRef.componentInstance.detailPC = detailPC;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/detail-pc', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/detail-pc', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
