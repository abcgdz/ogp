import { Component, OnInit } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiAlertService } from 'ng-jhipster';
import { IFonction, Fonction } from 'app/shared/model/fonction.model';
import { FonctionService } from './fonction.service';
import { IRessources } from 'app/shared/model/ressources.model';
import { RessourcesService } from 'app/entities/ressources';

@Component({
  selector: 'jhi-fonction-update',
  templateUrl: './fonction-update.component.html'
})
export class FonctionUpdateComponent implements OnInit {
  isSaving: boolean;

  ressources: IRessources[];

  editForm = this.fb.group({
    id: [],
    libelle: [],
    code: [],
    ressources: []
  });

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected fonctionService: FonctionService,
    protected ressourcesService: RessourcesService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ fonction }) => {
      this.updateForm(fonction);
    });
    this.ressourcesService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IRessources[]>) => mayBeOk.ok),
        map((response: HttpResponse<IRessources[]>) => response.body)
      )
      .subscribe((res: IRessources[]) => (this.ressources = res), (res: HttpErrorResponse) => this.onError(res.message));
  }

  updateForm(fonction: IFonction) {
    this.editForm.patchValue({
      id: fonction.id,
      libelle: fonction.libelle,
      code: fonction.code,
      ressources: fonction.ressources
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const fonction = this.createFromForm();
    if (fonction.id !== undefined) {
      this.subscribeToSaveResponse(this.fonctionService.update(fonction));
    } else {
      this.subscribeToSaveResponse(this.fonctionService.create(fonction));
    }
  }

  private createFromForm(): IFonction {
    return {
      ...new Fonction(),
      id: this.editForm.get(['id']).value,
      libelle: this.editForm.get(['libelle']).value,
      code: this.editForm.get(['code']).value,
      ressources: this.editForm.get(['ressources']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IFonction>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackRessourcesById(index: number, item: IRessources) {
    return item.id;
  }
}
