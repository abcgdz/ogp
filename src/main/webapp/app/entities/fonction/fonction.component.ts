import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { IFonction } from 'app/shared/model/fonction.model';
import { AccountService } from 'app/core';
import { FonctionService } from './fonction.service';

@Component({
  selector: 'jhi-fonction',
  templateUrl: './fonction.component.html'
})
export class FonctionComponent implements OnInit, OnDestroy {
  fonctions: IFonction[];
  currentAccount: any;
  eventSubscriber: Subscription;

  constructor(
    protected fonctionService: FonctionService,
    protected jhiAlertService: JhiAlertService,
    protected eventManager: JhiEventManager,
    protected accountService: AccountService
  ) {}

  loadAll() {
    this.fonctionService
      .query()
      .pipe(
        filter((res: HttpResponse<IFonction[]>) => res.ok),
        map((res: HttpResponse<IFonction[]>) => res.body)
      )
      .subscribe(
        (res: IFonction[]) => {
          this.fonctions = res;
        },
        (res: HttpErrorResponse) => this.onError(res.message)
      );
  }

  ngOnInit() {
    this.loadAll();
    this.accountService.identity().then(account => {
      this.currentAccount = account;
    });
    this.registerChangeInFonctions();
  }

  ngOnDestroy() {
    this.eventManager.destroy(this.eventSubscriber);
  }

  trackId(index: number, item: IFonction) {
    return item.id;
  }

  registerChangeInFonctions() {
    this.eventSubscriber = this.eventManager.subscribe('fonctionListModification', response => this.loadAll());
  }

  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }
}
