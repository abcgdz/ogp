import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { TypeDeProjet } from 'app/shared/model/type-de-projet.model';
import { TypeDeProjetService } from './type-de-projet.service';
import { TypeDeProjetComponent } from './type-de-projet.component';
import { TypeDeProjetDetailComponent } from './type-de-projet-detail.component';
import { TypeDeProjetUpdateComponent } from './type-de-projet-update.component';
import { TypeDeProjetDeletePopupComponent } from './type-de-projet-delete-dialog.component';
import { ITypeDeProjet } from 'app/shared/model/type-de-projet.model';

@Injectable({ providedIn: 'root' })
export class TypeDeProjetResolve implements Resolve<ITypeDeProjet> {
  constructor(private service: TypeDeProjetService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<ITypeDeProjet> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<TypeDeProjet>) => response.ok),
        map((typeDeProjet: HttpResponse<TypeDeProjet>) => typeDeProjet.body)
      );
    }
    return of(new TypeDeProjet());
  }
}

export const typeDeProjetRoute: Routes = [
  {
    path: '',
    component: TypeDeProjetComponent,
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'TypeDeProjets'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: TypeDeProjetDetailComponent,
    resolve: {
      typeDeProjet: TypeDeProjetResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'TypeDeProjets'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: TypeDeProjetUpdateComponent,
    resolve: {
      typeDeProjet: TypeDeProjetResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'TypeDeProjets'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: TypeDeProjetUpdateComponent,
    resolve: {
      typeDeProjet: TypeDeProjetResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'TypeDeProjets'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const typeDeProjetPopupRoute: Routes = [
  {
    path: ':id/delete',
    component: TypeDeProjetDeletePopupComponent,
    resolve: {
      typeDeProjet: TypeDeProjetResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'TypeDeProjets'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
