import { Component, OnInit } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { ITypeDeProjet, TypeDeProjet } from 'app/shared/model/type-de-projet.model';
import { TypeDeProjetService } from './type-de-projet.service';

@Component({
  selector: 'jhi-type-de-projet-update',
  templateUrl: './type-de-projet-update.component.html'
})
export class TypeDeProjetUpdateComponent implements OnInit {
  isSaving: boolean;

  editForm = this.fb.group({
    id: [],
    libelle: [],
    code: []
  });

  constructor(protected typeDeProjetService: TypeDeProjetService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ typeDeProjet }) => {
      this.updateForm(typeDeProjet);
    });
  }

  updateForm(typeDeProjet: ITypeDeProjet) {
    this.editForm.patchValue({
      id: typeDeProjet.id,
      libelle: typeDeProjet.libelle,
      code: typeDeProjet.code
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const typeDeProjet = this.createFromForm();
    if (typeDeProjet.id !== undefined) {
      this.subscribeToSaveResponse(this.typeDeProjetService.update(typeDeProjet));
    } else {
      this.subscribeToSaveResponse(this.typeDeProjetService.create(typeDeProjet));
    }
  }

  private createFromForm(): ITypeDeProjet {
    return {
      ...new TypeDeProjet(),
      id: this.editForm.get(['id']).value,
      libelle: this.editForm.get(['libelle']).value,
      code: this.editForm.get(['code']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ITypeDeProjet>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
}
