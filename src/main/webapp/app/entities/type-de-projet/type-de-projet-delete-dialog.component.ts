import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ITypeDeProjet } from 'app/shared/model/type-de-projet.model';
import { TypeDeProjetService } from './type-de-projet.service';

@Component({
  selector: 'jhi-type-de-projet-delete-dialog',
  templateUrl: './type-de-projet-delete-dialog.component.html'
})
export class TypeDeProjetDeleteDialogComponent {
  typeDeProjet: ITypeDeProjet;

  constructor(
    protected typeDeProjetService: TypeDeProjetService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.typeDeProjetService.delete(id).subscribe(response => {
      this.eventManager.broadcast({
        name: 'typeDeProjetListModification',
        content: 'Deleted an typeDeProjet'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-type-de-projet-delete-popup',
  template: ''
})
export class TypeDeProjetDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ typeDeProjet }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(TypeDeProjetDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
        this.ngbModalRef.componentInstance.typeDeProjet = typeDeProjet;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/type-de-projet', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/type-de-projet', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
