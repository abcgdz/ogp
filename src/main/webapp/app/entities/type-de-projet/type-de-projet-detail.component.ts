import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ITypeDeProjet } from 'app/shared/model/type-de-projet.model';

@Component({
  selector: 'jhi-type-de-projet-detail',
  templateUrl: './type-de-projet-detail.component.html'
})
export class TypeDeProjetDetailComponent implements OnInit {
  typeDeProjet: ITypeDeProjet;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ typeDeProjet }) => {
      this.typeDeProjet = typeDeProjet;
    });
  }

  previousState() {
    window.history.back();
  }
}
