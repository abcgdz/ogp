import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { ITypeDeProjet } from 'app/shared/model/type-de-projet.model';
import { AccountService } from 'app/core';
import { TypeDeProjetService } from './type-de-projet.service';

@Component({
  selector: 'jhi-type-de-projet',
  templateUrl: './type-de-projet.component.html'
})
export class TypeDeProjetComponent implements OnInit, OnDestroy {
  typeDeProjets: ITypeDeProjet[];
  currentAccount: any;
  eventSubscriber: Subscription;

  constructor(
    protected typeDeProjetService: TypeDeProjetService,
    protected jhiAlertService: JhiAlertService,
    protected eventManager: JhiEventManager,
    protected accountService: AccountService
  ) {}

  loadAll() {
    this.typeDeProjetService
      .query()
      .pipe(
        filter((res: HttpResponse<ITypeDeProjet[]>) => res.ok),
        map((res: HttpResponse<ITypeDeProjet[]>) => res.body)
      )
      .subscribe(
        (res: ITypeDeProjet[]) => {
          this.typeDeProjets = res;
        },
        (res: HttpErrorResponse) => this.onError(res.message)
      );
  }

  ngOnInit() {
    this.loadAll();
    this.accountService.identity().then(account => {
      this.currentAccount = account;
    });
    this.registerChangeInTypeDeProjets();
  }

  ngOnDestroy() {
    this.eventManager.destroy(this.eventSubscriber);
  }

  trackId(index: number, item: ITypeDeProjet) {
    return item.id;
  }

  registerChangeInTypeDeProjets() {
    this.eventSubscriber = this.eventManager.subscribe('typeDeProjetListModification', response => this.loadAll());
  }

  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }
}
