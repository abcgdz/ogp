import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { OgpSharedModule } from 'app/shared';
import {
  TypeDeProjetComponent,
  TypeDeProjetDetailComponent,
  TypeDeProjetUpdateComponent,
  TypeDeProjetDeletePopupComponent,
  TypeDeProjetDeleteDialogComponent,
  typeDeProjetRoute,
  typeDeProjetPopupRoute
} from './';

const ENTITY_STATES = [...typeDeProjetRoute, ...typeDeProjetPopupRoute];

@NgModule({
  imports: [OgpSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    TypeDeProjetComponent,
    TypeDeProjetDetailComponent,
    TypeDeProjetUpdateComponent,
    TypeDeProjetDeleteDialogComponent,
    TypeDeProjetDeletePopupComponent
  ],
  entryComponents: [
    TypeDeProjetComponent,
    TypeDeProjetUpdateComponent,
    TypeDeProjetDeleteDialogComponent,
    TypeDeProjetDeletePopupComponent
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class OgpTypeDeProjetModule {}
