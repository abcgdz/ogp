import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'client',
        loadChildren: () => import('./client/client.module').then(m => m.OgpClientModule)
      },
      {
        path: 'projet',
        loadChildren: () => import('./projet/projet.module').then(m => m.OgpProjetModule)
      },
      {
        path: 'bu',
        loadChildren: () => import('./bu/bu.module').then(m => m.OgpBUModule)
      },
      {
        path: 'pc',
        loadChildren: () => import('./pc/pc.module').then(m => m.OgpPcModule)
      },
      {
        path: 'detail-pc',
        loadChildren: () => import('./detail-pc/detail-pc.module').then(m => m.OgpDetailPCModule)
      },
      {
        path: 'fonction',
        loadChildren: () => import('./fonction/fonction.module').then(m => m.OgpFonctionModule)
      },
      {
        path: 'ressources',
        loadChildren: () => import('./ressources/ressources.module').then(m => m.OgpRessourcesModule)
      },
      {
        path: 'statut',
        loadChildren: () => import('./statut/statut.module').then(m => m.OgpStatutModule)
      },
      {
        path: 'type-de-projet',
        loadChildren: () => import('./type-de-projet/type-de-projet.module').then(m => m.OgpTypeDeProjetModule)
      },
      {
        path: 'duree',
        loadChildren: () => import('./duree/duree.module').then(m => m.OgpDureeModule)
      }
      /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
    ])
  ],
  declarations: [],
  entryComponents: [],
  providers: [],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class OgpEntityModule {}
