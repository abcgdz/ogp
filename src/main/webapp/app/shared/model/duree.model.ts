import { IDetailPC } from 'app/shared/model/detail-pc.model';

export interface IDuree {
  id?: number;
  jour?: number;
  mois?: string;
  detailPC?: IDetailPC;
}

export class Duree implements IDuree {
  constructor(public id?: number, public jour?: number, public mois?: string, public detailPC?: IDetailPC) {}
}
