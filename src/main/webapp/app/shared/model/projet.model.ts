import { IClient } from 'app/shared/model/client.model';
import { ITypeDeProjet } from 'app/shared/model/type-de-projet.model';
import { IDetailPC } from 'app/shared/model/detail-pc.model';

export interface IProjet {
  id?: number;
  acronyme?: string;
  probabiliteGain?: number;
  chargeJh?: number;
  moisDeDemarrage?: string;
  duree?: string;
  moisDeCloture?: string;
  a?: IClient;
  typ?: ITypeDeProjet;
  detailPC?: IDetailPC;
}

export class Projet implements IProjet {
  constructor(
    public id?: number,
    public acronyme?: string,
    public probabiliteGain?: number,
    public chargeJh?: number,
    public moisDeDemarrage?: string,
    public duree?: string,
    public moisDeCloture?: string,
    public a?: IClient,
    public typ?: ITypeDeProjet,
    public detailPC?: IDetailPC
  ) {}
}
