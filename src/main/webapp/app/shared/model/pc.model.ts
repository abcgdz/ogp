import { IDetailPC } from 'app/shared/model/detail-pc.model';

export interface IPc {
  id?: number;
  libelle?: string;
  code?: number;
  detailPC?: IDetailPC;
}

export class Pc implements IPc {
  constructor(public id?: number, public libelle?: string, public code?: number, public detailPC?: IDetailPC) {}
}
