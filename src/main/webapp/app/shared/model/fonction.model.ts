import { IRessources } from 'app/shared/model/ressources.model';

export interface IFonction {
  id?: number;
  libelle?: string;
  code?: number;
  ressources?: IRessources;
}

export class Fonction implements IFonction {
  constructor(public id?: number, public libelle?: string, public code?: number, public ressources?: IRessources) {}
}
