import { IProjet } from 'app/shared/model/projet.model';
import { IBU } from 'app/shared/model/bu.model';
import { IPc } from 'app/shared/model/pc.model';
import { IDuree } from 'app/shared/model/duree.model';

export interface IDetailPC {
  id?: number;
  libelle?: string;
  code?: number;
  role?: string;
  ts?: IProjet[];
  tis?: IBU[];
  kjks?: IPc[];
  fvgs?: IDuree[];
}

export class DetailPC implements IDetailPC {
  constructor(
    public id?: number,
    public libelle?: string,
    public code?: number,
    public role?: string,
    public ts?: IProjet[],
    public tis?: IBU[],
    public kjks?: IPc[],
    public fvgs?: IDuree[]
  ) {}
}
