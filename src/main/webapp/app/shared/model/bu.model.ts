import { IProjet } from 'app/shared/model/projet.model';
import { IRessources } from 'app/shared/model/ressources.model';
import { IDetailPC } from 'app/shared/model/detail-pc.model';

export interface IBU {
  id?: number;
  nom?: string;
  prenom?: string;
  l?: IProjet;
  dscs?: IRessources[];
  detailPC?: IDetailPC;
}

export class BU implements IBU {
  constructor(
    public id?: number,
    public nom?: string,
    public prenom?: string,
    public l?: IProjet,
    public dscs?: IRessources[],
    public detailPC?: IDetailPC
  ) {}
}
