import { IBU } from 'app/shared/model/bu.model';
import { IStatut } from 'app/shared/model/statut.model';
import { IFonction } from 'app/shared/model/fonction.model';

export interface IRessources {
  id?: number;
  nom?: string;
  prenom?: string;
  acronyme?: string;
  bU?: IBU;
  ties?: IStatut[];
  svs?: IFonction[];
}

export class Ressources implements IRessources {
  constructor(
    public id?: number,
    public nom?: string,
    public prenom?: string,
    public acronyme?: string,
    public bU?: IBU,
    public ties?: IStatut[],
    public svs?: IFonction[]
  ) {}
}
