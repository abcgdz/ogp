import { IRessources } from 'app/shared/model/ressources.model';

export interface IStatut {
  id?: number;
  libelle?: string;
  code?: number;
  ressources?: IRessources;
}

export class Statut implements IStatut {
  constructor(public id?: number, public libelle?: string, public code?: number, public ressources?: IRessources) {}
}
