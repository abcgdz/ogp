export interface ITypeDeProjet {
  id?: number;
  libelle?: string;
  code?: number;
}

export class TypeDeProjet implements ITypeDeProjet {
  constructor(public id?: number, public libelle?: string, public code?: number) {}
}
