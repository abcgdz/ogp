import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { OgpSharedCommonModule, JhiLoginModalComponent, HasAnyAuthorityDirective } from './';

@NgModule({
  imports: [OgpSharedCommonModule],
  declarations: [JhiLoginModalComponent, HasAnyAuthorityDirective],
  entryComponents: [JhiLoginModalComponent],
  exports: [OgpSharedCommonModule, JhiLoginModalComponent, HasAnyAuthorityDirective],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class OgpSharedModule {
  static forRoot() {
    return {
      ngModule: OgpSharedModule
    };
  }
}
