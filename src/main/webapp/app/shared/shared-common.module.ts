import { NgModule } from '@angular/core';

import { OgpSharedLibsModule, JhiAlertComponent, JhiAlertErrorComponent } from './';

@NgModule({
  imports: [OgpSharedLibsModule],
  declarations: [JhiAlertComponent, JhiAlertErrorComponent],
  exports: [OgpSharedLibsModule, JhiAlertComponent, JhiAlertErrorComponent]
})
export class OgpSharedCommonModule {}
