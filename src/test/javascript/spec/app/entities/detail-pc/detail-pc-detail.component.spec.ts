/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { OgpTestModule } from '../../../test.module';
import { DetailPCDetailComponent } from 'app/entities/detail-pc/detail-pc-detail.component';
import { DetailPC } from 'app/shared/model/detail-pc.model';

describe('Component Tests', () => {
  describe('DetailPC Management Detail Component', () => {
    let comp: DetailPCDetailComponent;
    let fixture: ComponentFixture<DetailPCDetailComponent>;
    const route = ({ data: of({ detailPC: new DetailPC(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [OgpTestModule],
        declarations: [DetailPCDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(DetailPCDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(DetailPCDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should call load all on init', () => {
        // GIVEN

        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.detailPC).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
