/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { OgpTestModule } from '../../../test.module';
import { DetailPCDeleteDialogComponent } from 'app/entities/detail-pc/detail-pc-delete-dialog.component';
import { DetailPCService } from 'app/entities/detail-pc/detail-pc.service';

describe('Component Tests', () => {
  describe('DetailPC Management Delete Component', () => {
    let comp: DetailPCDeleteDialogComponent;
    let fixture: ComponentFixture<DetailPCDeleteDialogComponent>;
    let service: DetailPCService;
    let mockEventManager: any;
    let mockActiveModal: any;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [OgpTestModule],
        declarations: [DetailPCDeleteDialogComponent]
      })
        .overrideTemplate(DetailPCDeleteDialogComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(DetailPCDeleteDialogComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(DetailPCService);
      mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
      mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
    });

    describe('confirmDelete', () => {
      it('Should call delete service on confirmDelete', inject(
        [],
        fakeAsync(() => {
          // GIVEN
          spyOn(service, 'delete').and.returnValue(of({}));

          // WHEN
          comp.confirmDelete(123);
          tick();

          // THEN
          expect(service.delete).toHaveBeenCalledWith(123);
          expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
          expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
        })
      ));
    });
  });
});
