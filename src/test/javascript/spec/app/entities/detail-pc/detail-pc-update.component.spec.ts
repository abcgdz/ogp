/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { Observable, of } from 'rxjs';

import { OgpTestModule } from '../../../test.module';
import { DetailPCUpdateComponent } from 'app/entities/detail-pc/detail-pc-update.component';
import { DetailPCService } from 'app/entities/detail-pc/detail-pc.service';
import { DetailPC } from 'app/shared/model/detail-pc.model';

describe('Component Tests', () => {
  describe('DetailPC Management Update Component', () => {
    let comp: DetailPCUpdateComponent;
    let fixture: ComponentFixture<DetailPCUpdateComponent>;
    let service: DetailPCService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [OgpTestModule],
        declarations: [DetailPCUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(DetailPCUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(DetailPCUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(DetailPCService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new DetailPC(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new DetailPC();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
