/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Observable, of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { OgpTestModule } from '../../../test.module';
import { DetailPCComponent } from 'app/entities/detail-pc/detail-pc.component';
import { DetailPCService } from 'app/entities/detail-pc/detail-pc.service';
import { DetailPC } from 'app/shared/model/detail-pc.model';

describe('Component Tests', () => {
  describe('DetailPC Management Component', () => {
    let comp: DetailPCComponent;
    let fixture: ComponentFixture<DetailPCComponent>;
    let service: DetailPCService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [OgpTestModule],
        declarations: [DetailPCComponent],
        providers: []
      })
        .overrideTemplate(DetailPCComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(DetailPCComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(DetailPCService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new DetailPC(123)],
            headers
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.detailPCS[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
  });
});
