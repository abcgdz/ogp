/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { OgpTestModule } from '../../../test.module';
import { RessourcesDeleteDialogComponent } from 'app/entities/ressources/ressources-delete-dialog.component';
import { RessourcesService } from 'app/entities/ressources/ressources.service';

describe('Component Tests', () => {
  describe('Ressources Management Delete Component', () => {
    let comp: RessourcesDeleteDialogComponent;
    let fixture: ComponentFixture<RessourcesDeleteDialogComponent>;
    let service: RessourcesService;
    let mockEventManager: any;
    let mockActiveModal: any;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [OgpTestModule],
        declarations: [RessourcesDeleteDialogComponent]
      })
        .overrideTemplate(RessourcesDeleteDialogComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(RessourcesDeleteDialogComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(RessourcesService);
      mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
      mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
    });

    describe('confirmDelete', () => {
      it('Should call delete service on confirmDelete', inject(
        [],
        fakeAsync(() => {
          // GIVEN
          spyOn(service, 'delete').and.returnValue(of({}));

          // WHEN
          comp.confirmDelete(123);
          tick();

          // THEN
          expect(service.delete).toHaveBeenCalledWith(123);
          expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
          expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
        })
      ));
    });
  });
});
