/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Observable, of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { OgpTestModule } from '../../../test.module';
import { RessourcesComponent } from 'app/entities/ressources/ressources.component';
import { RessourcesService } from 'app/entities/ressources/ressources.service';
import { Ressources } from 'app/shared/model/ressources.model';

describe('Component Tests', () => {
  describe('Ressources Management Component', () => {
    let comp: RessourcesComponent;
    let fixture: ComponentFixture<RessourcesComponent>;
    let service: RessourcesService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [OgpTestModule],
        declarations: [RessourcesComponent],
        providers: []
      })
        .overrideTemplate(RessourcesComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(RessourcesComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(RessourcesService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new Ressources(123)],
            headers
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.ressources[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
  });
});
