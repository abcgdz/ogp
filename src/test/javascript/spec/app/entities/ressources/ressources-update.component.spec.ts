/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { Observable, of } from 'rxjs';

import { OgpTestModule } from '../../../test.module';
import { RessourcesUpdateComponent } from 'app/entities/ressources/ressources-update.component';
import { RessourcesService } from 'app/entities/ressources/ressources.service';
import { Ressources } from 'app/shared/model/ressources.model';

describe('Component Tests', () => {
  describe('Ressources Management Update Component', () => {
    let comp: RessourcesUpdateComponent;
    let fixture: ComponentFixture<RessourcesUpdateComponent>;
    let service: RessourcesService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [OgpTestModule],
        declarations: [RessourcesUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(RessourcesUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(RessourcesUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(RessourcesService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new Ressources(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new Ressources();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
