/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { OgpTestModule } from '../../../test.module';
import { RessourcesDetailComponent } from 'app/entities/ressources/ressources-detail.component';
import { Ressources } from 'app/shared/model/ressources.model';

describe('Component Tests', () => {
  describe('Ressources Management Detail Component', () => {
    let comp: RessourcesDetailComponent;
    let fixture: ComponentFixture<RessourcesDetailComponent>;
    const route = ({ data: of({ ressources: new Ressources(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [OgpTestModule],
        declarations: [RessourcesDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(RessourcesDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(RessourcesDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should call load all on init', () => {
        // GIVEN

        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.ressources).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
