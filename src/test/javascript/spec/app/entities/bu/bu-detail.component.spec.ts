/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { OgpTestModule } from '../../../test.module';
import { BUDetailComponent } from 'app/entities/bu/bu-detail.component';
import { BU } from 'app/shared/model/bu.model';

describe('Component Tests', () => {
  describe('BU Management Detail Component', () => {
    let comp: BUDetailComponent;
    let fixture: ComponentFixture<BUDetailComponent>;
    const route = ({ data: of({ bU: new BU(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [OgpTestModule],
        declarations: [BUDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(BUDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(BUDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should call load all on init', () => {
        // GIVEN

        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.bU).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
