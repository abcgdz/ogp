/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Observable, of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { OgpTestModule } from '../../../test.module';
import { BUComponent } from 'app/entities/bu/bu.component';
import { BUService } from 'app/entities/bu/bu.service';
import { BU } from 'app/shared/model/bu.model';

describe('Component Tests', () => {
  describe('BU Management Component', () => {
    let comp: BUComponent;
    let fixture: ComponentFixture<BUComponent>;
    let service: BUService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [OgpTestModule],
        declarations: [BUComponent],
        providers: []
      })
        .overrideTemplate(BUComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(BUComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(BUService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new BU(123)],
            headers
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.bUS[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
  });
});
