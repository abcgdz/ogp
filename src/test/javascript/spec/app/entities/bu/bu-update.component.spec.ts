/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { Observable, of } from 'rxjs';

import { OgpTestModule } from '../../../test.module';
import { BUUpdateComponent } from 'app/entities/bu/bu-update.component';
import { BUService } from 'app/entities/bu/bu.service';
import { BU } from 'app/shared/model/bu.model';

describe('Component Tests', () => {
  describe('BU Management Update Component', () => {
    let comp: BUUpdateComponent;
    let fixture: ComponentFixture<BUUpdateComponent>;
    let service: BUService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [OgpTestModule],
        declarations: [BUUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(BUUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(BUUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(BUService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new BU(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new BU();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
