/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { OgpTestModule } from '../../../test.module';
import { BUDeleteDialogComponent } from 'app/entities/bu/bu-delete-dialog.component';
import { BUService } from 'app/entities/bu/bu.service';

describe('Component Tests', () => {
  describe('BU Management Delete Component', () => {
    let comp: BUDeleteDialogComponent;
    let fixture: ComponentFixture<BUDeleteDialogComponent>;
    let service: BUService;
    let mockEventManager: any;
    let mockActiveModal: any;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [OgpTestModule],
        declarations: [BUDeleteDialogComponent]
      })
        .overrideTemplate(BUDeleteDialogComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(BUDeleteDialogComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(BUService);
      mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
      mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
    });

    describe('confirmDelete', () => {
      it('Should call delete service on confirmDelete', inject(
        [],
        fakeAsync(() => {
          // GIVEN
          spyOn(service, 'delete').and.returnValue(of({}));

          // WHEN
          comp.confirmDelete(123);
          tick();

          // THEN
          expect(service.delete).toHaveBeenCalledWith(123);
          expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
          expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
        })
      ));
    });
  });
});
