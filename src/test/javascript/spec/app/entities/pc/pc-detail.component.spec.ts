/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { OgpTestModule } from '../../../test.module';
import { PcDetailComponent } from 'app/entities/pc/pc-detail.component';
import { Pc } from 'app/shared/model/pc.model';

describe('Component Tests', () => {
  describe('Pc Management Detail Component', () => {
    let comp: PcDetailComponent;
    let fixture: ComponentFixture<PcDetailComponent>;
    const route = ({ data: of({ pc: new Pc(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [OgpTestModule],
        declarations: [PcDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(PcDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(PcDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should call load all on init', () => {
        // GIVEN

        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.pc).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
