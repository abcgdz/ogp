/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Observable, of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { OgpTestModule } from '../../../test.module';
import { PcComponent } from 'app/entities/pc/pc.component';
import { PcService } from 'app/entities/pc/pc.service';
import { Pc } from 'app/shared/model/pc.model';

describe('Component Tests', () => {
  describe('Pc Management Component', () => {
    let comp: PcComponent;
    let fixture: ComponentFixture<PcComponent>;
    let service: PcService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [OgpTestModule],
        declarations: [PcComponent],
        providers: []
      })
        .overrideTemplate(PcComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(PcComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(PcService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new Pc(123)],
            headers
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.pcs[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
  });
});
