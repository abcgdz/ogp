/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { OgpTestModule } from '../../../test.module';
import { PcDeleteDialogComponent } from 'app/entities/pc/pc-delete-dialog.component';
import { PcService } from 'app/entities/pc/pc.service';

describe('Component Tests', () => {
  describe('Pc Management Delete Component', () => {
    let comp: PcDeleteDialogComponent;
    let fixture: ComponentFixture<PcDeleteDialogComponent>;
    let service: PcService;
    let mockEventManager: any;
    let mockActiveModal: any;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [OgpTestModule],
        declarations: [PcDeleteDialogComponent]
      })
        .overrideTemplate(PcDeleteDialogComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(PcDeleteDialogComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(PcService);
      mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
      mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
    });

    describe('confirmDelete', () => {
      it('Should call delete service on confirmDelete', inject(
        [],
        fakeAsync(() => {
          // GIVEN
          spyOn(service, 'delete').and.returnValue(of({}));

          // WHEN
          comp.confirmDelete(123);
          tick();

          // THEN
          expect(service.delete).toHaveBeenCalledWith(123);
          expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
          expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
        })
      ));
    });
  });
});
