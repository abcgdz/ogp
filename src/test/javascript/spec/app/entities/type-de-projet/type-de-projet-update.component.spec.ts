/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { Observable, of } from 'rxjs';

import { OgpTestModule } from '../../../test.module';
import { TypeDeProjetUpdateComponent } from 'app/entities/type-de-projet/type-de-projet-update.component';
import { TypeDeProjetService } from 'app/entities/type-de-projet/type-de-projet.service';
import { TypeDeProjet } from 'app/shared/model/type-de-projet.model';

describe('Component Tests', () => {
  describe('TypeDeProjet Management Update Component', () => {
    let comp: TypeDeProjetUpdateComponent;
    let fixture: ComponentFixture<TypeDeProjetUpdateComponent>;
    let service: TypeDeProjetService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [OgpTestModule],
        declarations: [TypeDeProjetUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(TypeDeProjetUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(TypeDeProjetUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(TypeDeProjetService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new TypeDeProjet(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new TypeDeProjet();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
