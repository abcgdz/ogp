/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Observable, of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { OgpTestModule } from '../../../test.module';
import { TypeDeProjetComponent } from 'app/entities/type-de-projet/type-de-projet.component';
import { TypeDeProjetService } from 'app/entities/type-de-projet/type-de-projet.service';
import { TypeDeProjet } from 'app/shared/model/type-de-projet.model';

describe('Component Tests', () => {
  describe('TypeDeProjet Management Component', () => {
    let comp: TypeDeProjetComponent;
    let fixture: ComponentFixture<TypeDeProjetComponent>;
    let service: TypeDeProjetService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [OgpTestModule],
        declarations: [TypeDeProjetComponent],
        providers: []
      })
        .overrideTemplate(TypeDeProjetComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(TypeDeProjetComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(TypeDeProjetService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new TypeDeProjet(123)],
            headers
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.typeDeProjets[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
  });
});
