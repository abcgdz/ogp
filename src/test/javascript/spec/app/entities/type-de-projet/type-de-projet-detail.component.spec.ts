/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { OgpTestModule } from '../../../test.module';
import { TypeDeProjetDetailComponent } from 'app/entities/type-de-projet/type-de-projet-detail.component';
import { TypeDeProjet } from 'app/shared/model/type-de-projet.model';

describe('Component Tests', () => {
  describe('TypeDeProjet Management Detail Component', () => {
    let comp: TypeDeProjetDetailComponent;
    let fixture: ComponentFixture<TypeDeProjetDetailComponent>;
    const route = ({ data: of({ typeDeProjet: new TypeDeProjet(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [OgpTestModule],
        declarations: [TypeDeProjetDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(TypeDeProjetDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(TypeDeProjetDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should call load all on init', () => {
        // GIVEN

        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.typeDeProjet).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
