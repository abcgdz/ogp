/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { Observable, of } from 'rxjs';

import { OgpTestModule } from '../../../test.module';
import { DureeUpdateComponent } from 'app/entities/duree/duree-update.component';
import { DureeService } from 'app/entities/duree/duree.service';
import { Duree } from 'app/shared/model/duree.model';

describe('Component Tests', () => {
  describe('Duree Management Update Component', () => {
    let comp: DureeUpdateComponent;
    let fixture: ComponentFixture<DureeUpdateComponent>;
    let service: DureeService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [OgpTestModule],
        declarations: [DureeUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(DureeUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(DureeUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(DureeService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new Duree(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new Duree();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
