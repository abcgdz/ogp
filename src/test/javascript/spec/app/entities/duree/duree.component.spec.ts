/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Observable, of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { OgpTestModule } from '../../../test.module';
import { DureeComponent } from 'app/entities/duree/duree.component';
import { DureeService } from 'app/entities/duree/duree.service';
import { Duree } from 'app/shared/model/duree.model';

describe('Component Tests', () => {
  describe('Duree Management Component', () => {
    let comp: DureeComponent;
    let fixture: ComponentFixture<DureeComponent>;
    let service: DureeService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [OgpTestModule],
        declarations: [DureeComponent],
        providers: []
      })
        .overrideTemplate(DureeComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(DureeComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(DureeService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new Duree(123)],
            headers
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.durees[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
  });
});
