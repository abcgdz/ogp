/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { OgpTestModule } from '../../../test.module';
import { DureeDetailComponent } from 'app/entities/duree/duree-detail.component';
import { Duree } from 'app/shared/model/duree.model';

describe('Component Tests', () => {
  describe('Duree Management Detail Component', () => {
    let comp: DureeDetailComponent;
    let fixture: ComponentFixture<DureeDetailComponent>;
    const route = ({ data: of({ duree: new Duree(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [OgpTestModule],
        declarations: [DureeDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(DureeDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(DureeDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should call load all on init', () => {
        // GIVEN

        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.duree).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
