package af.web.rest;

import af.OgpApp;
import af.domain.Ressources;
import af.repository.RessourcesRepository;
import af.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;

import static af.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link RessourcesResource} REST controller.
 */
@SpringBootTest(classes = OgpApp.class)
public class RessourcesResourceIT {

    private static final String DEFAULT_NOM = "AAAAAAAAAA";
    private static final String UPDATED_NOM = "BBBBBBBBBB";

    private static final String DEFAULT_PRENOM = "AAAAAAAAAA";
    private static final String UPDATED_PRENOM = "BBBBBBBBBB";

    private static final String DEFAULT_ACRONYME = "AAAAAAAAAA";
    private static final String UPDATED_ACRONYME = "BBBBBBBBBB";

    @Autowired
    private RessourcesRepository ressourcesRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restRessourcesMockMvc;

    private Ressources ressources;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final RessourcesResource ressourcesResource = new RessourcesResource(ressourcesRepository);
        this.restRessourcesMockMvc = MockMvcBuilders.standaloneSetup(ressourcesResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Ressources createEntity(EntityManager em) {
        Ressources ressources = new Ressources()
            .nom(DEFAULT_NOM)
            .prenom(DEFAULT_PRENOM)
            .acronyme(DEFAULT_ACRONYME);
        return ressources;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Ressources createUpdatedEntity(EntityManager em) {
        Ressources ressources = new Ressources()
            .nom(UPDATED_NOM)
            .prenom(UPDATED_PRENOM)
            .acronyme(UPDATED_ACRONYME);
        return ressources;
    }

    @BeforeEach
    public void initTest() {
        ressources = createEntity(em);
    }

    @Test
    @Transactional
    public void createRessources() throws Exception {
        int databaseSizeBeforeCreate = ressourcesRepository.findAll().size();

        // Create the Ressources
        restRessourcesMockMvc.perform(post("/api/ressources")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(ressources)))
            .andExpect(status().isCreated());

        // Validate the Ressources in the database
        List<Ressources> ressourcesList = ressourcesRepository.findAll();
        assertThat(ressourcesList).hasSize(databaseSizeBeforeCreate + 1);
        Ressources testRessources = ressourcesList.get(ressourcesList.size() - 1);
        assertThat(testRessources.getNom()).isEqualTo(DEFAULT_NOM);
        assertThat(testRessources.getPrenom()).isEqualTo(DEFAULT_PRENOM);
        assertThat(testRessources.getAcronyme()).isEqualTo(DEFAULT_ACRONYME);
    }

    @Test
    @Transactional
    public void createRessourcesWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = ressourcesRepository.findAll().size();

        // Create the Ressources with an existing ID
        ressources.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restRessourcesMockMvc.perform(post("/api/ressources")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(ressources)))
            .andExpect(status().isBadRequest());

        // Validate the Ressources in the database
        List<Ressources> ressourcesList = ressourcesRepository.findAll();
        assertThat(ressourcesList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllRessources() throws Exception {
        // Initialize the database
        ressourcesRepository.saveAndFlush(ressources);

        // Get all the ressourcesList
        restRessourcesMockMvc.perform(get("/api/ressources?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(ressources.getId().intValue())))
            .andExpect(jsonPath("$.[*].nom").value(hasItem(DEFAULT_NOM.toString())))
            .andExpect(jsonPath("$.[*].prenom").value(hasItem(DEFAULT_PRENOM.toString())))
            .andExpect(jsonPath("$.[*].acronyme").value(hasItem(DEFAULT_ACRONYME.toString())));
    }
    
    @Test
    @Transactional
    public void getRessources() throws Exception {
        // Initialize the database
        ressourcesRepository.saveAndFlush(ressources);

        // Get the ressources
        restRessourcesMockMvc.perform(get("/api/ressources/{id}", ressources.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(ressources.getId().intValue()))
            .andExpect(jsonPath("$.nom").value(DEFAULT_NOM.toString()))
            .andExpect(jsonPath("$.prenom").value(DEFAULT_PRENOM.toString()))
            .andExpect(jsonPath("$.acronyme").value(DEFAULT_ACRONYME.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingRessources() throws Exception {
        // Get the ressources
        restRessourcesMockMvc.perform(get("/api/ressources/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateRessources() throws Exception {
        // Initialize the database
        ressourcesRepository.saveAndFlush(ressources);

        int databaseSizeBeforeUpdate = ressourcesRepository.findAll().size();

        // Update the ressources
        Ressources updatedRessources = ressourcesRepository.findById(ressources.getId()).get();
        // Disconnect from session so that the updates on updatedRessources are not directly saved in db
        em.detach(updatedRessources);
        updatedRessources
            .nom(UPDATED_NOM)
            .prenom(UPDATED_PRENOM)
            .acronyme(UPDATED_ACRONYME);

        restRessourcesMockMvc.perform(put("/api/ressources")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedRessources)))
            .andExpect(status().isOk());

        // Validate the Ressources in the database
        List<Ressources> ressourcesList = ressourcesRepository.findAll();
        assertThat(ressourcesList).hasSize(databaseSizeBeforeUpdate);
        Ressources testRessources = ressourcesList.get(ressourcesList.size() - 1);
        assertThat(testRessources.getNom()).isEqualTo(UPDATED_NOM);
        assertThat(testRessources.getPrenom()).isEqualTo(UPDATED_PRENOM);
        assertThat(testRessources.getAcronyme()).isEqualTo(UPDATED_ACRONYME);
    }

    @Test
    @Transactional
    public void updateNonExistingRessources() throws Exception {
        int databaseSizeBeforeUpdate = ressourcesRepository.findAll().size();

        // Create the Ressources

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restRessourcesMockMvc.perform(put("/api/ressources")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(ressources)))
            .andExpect(status().isBadRequest());

        // Validate the Ressources in the database
        List<Ressources> ressourcesList = ressourcesRepository.findAll();
        assertThat(ressourcesList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteRessources() throws Exception {
        // Initialize the database
        ressourcesRepository.saveAndFlush(ressources);

        int databaseSizeBeforeDelete = ressourcesRepository.findAll().size();

        // Delete the ressources
        restRessourcesMockMvc.perform(delete("/api/ressources/{id}", ressources.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Ressources> ressourcesList = ressourcesRepository.findAll();
        assertThat(ressourcesList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Ressources.class);
        Ressources ressources1 = new Ressources();
        ressources1.setId(1L);
        Ressources ressources2 = new Ressources();
        ressources2.setId(ressources1.getId());
        assertThat(ressources1).isEqualTo(ressources2);
        ressources2.setId(2L);
        assertThat(ressources1).isNotEqualTo(ressources2);
        ressources1.setId(null);
        assertThat(ressources1).isNotEqualTo(ressources2);
    }
}
