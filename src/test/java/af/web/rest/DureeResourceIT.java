package af.web.rest;

import af.OgpApp;
import af.domain.Duree;
import af.repository.DureeRepository;
import af.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;

import static af.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link DureeResource} REST controller.
 */
@SpringBootTest(classes = OgpApp.class)
public class DureeResourceIT {

    private static final Integer DEFAULT_JOUR = 1;
    private static final Integer UPDATED_JOUR = 2;
    private static final Integer SMALLER_JOUR = 1 - 1;

    private static final String DEFAULT_MOIS = "AAAAAAAAAA";
    private static final String UPDATED_MOIS = "BBBBBBBBBB";

    @Autowired
    private DureeRepository dureeRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restDureeMockMvc;

    private Duree duree;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final DureeResource dureeResource = new DureeResource(dureeRepository);
        this.restDureeMockMvc = MockMvcBuilders.standaloneSetup(dureeResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Duree createEntity(EntityManager em) {
        Duree duree = new Duree()
            .jour(DEFAULT_JOUR)
            .mois(DEFAULT_MOIS);
        return duree;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Duree createUpdatedEntity(EntityManager em) {
        Duree duree = new Duree()
            .jour(UPDATED_JOUR)
            .mois(UPDATED_MOIS);
        return duree;
    }

    @BeforeEach
    public void initTest() {
        duree = createEntity(em);
    }

    @Test
    @Transactional
    public void createDuree() throws Exception {
        int databaseSizeBeforeCreate = dureeRepository.findAll().size();

        // Create the Duree
        restDureeMockMvc.perform(post("/api/durees")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(duree)))
            .andExpect(status().isCreated());

        // Validate the Duree in the database
        List<Duree> dureeList = dureeRepository.findAll();
        assertThat(dureeList).hasSize(databaseSizeBeforeCreate + 1);
        Duree testDuree = dureeList.get(dureeList.size() - 1);
        assertThat(testDuree.getJour()).isEqualTo(DEFAULT_JOUR);
        assertThat(testDuree.getMois()).isEqualTo(DEFAULT_MOIS);
    }

    @Test
    @Transactional
    public void createDureeWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = dureeRepository.findAll().size();

        // Create the Duree with an existing ID
        duree.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restDureeMockMvc.perform(post("/api/durees")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(duree)))
            .andExpect(status().isBadRequest());

        // Validate the Duree in the database
        List<Duree> dureeList = dureeRepository.findAll();
        assertThat(dureeList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllDurees() throws Exception {
        // Initialize the database
        dureeRepository.saveAndFlush(duree);

        // Get all the dureeList
        restDureeMockMvc.perform(get("/api/durees?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(duree.getId().intValue())))
            .andExpect(jsonPath("$.[*].jour").value(hasItem(DEFAULT_JOUR)))
            .andExpect(jsonPath("$.[*].mois").value(hasItem(DEFAULT_MOIS.toString())));
    }
    
    @Test
    @Transactional
    public void getDuree() throws Exception {
        // Initialize the database
        dureeRepository.saveAndFlush(duree);

        // Get the duree
        restDureeMockMvc.perform(get("/api/durees/{id}", duree.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(duree.getId().intValue()))
            .andExpect(jsonPath("$.jour").value(DEFAULT_JOUR))
            .andExpect(jsonPath("$.mois").value(DEFAULT_MOIS.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingDuree() throws Exception {
        // Get the duree
        restDureeMockMvc.perform(get("/api/durees/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateDuree() throws Exception {
        // Initialize the database
        dureeRepository.saveAndFlush(duree);

        int databaseSizeBeforeUpdate = dureeRepository.findAll().size();

        // Update the duree
        Duree updatedDuree = dureeRepository.findById(duree.getId()).get();
        // Disconnect from session so that the updates on updatedDuree are not directly saved in db
        em.detach(updatedDuree);
        updatedDuree
            .jour(UPDATED_JOUR)
            .mois(UPDATED_MOIS);

        restDureeMockMvc.perform(put("/api/durees")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedDuree)))
            .andExpect(status().isOk());

        // Validate the Duree in the database
        List<Duree> dureeList = dureeRepository.findAll();
        assertThat(dureeList).hasSize(databaseSizeBeforeUpdate);
        Duree testDuree = dureeList.get(dureeList.size() - 1);
        assertThat(testDuree.getJour()).isEqualTo(UPDATED_JOUR);
        assertThat(testDuree.getMois()).isEqualTo(UPDATED_MOIS);
    }

    @Test
    @Transactional
    public void updateNonExistingDuree() throws Exception {
        int databaseSizeBeforeUpdate = dureeRepository.findAll().size();

        // Create the Duree

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restDureeMockMvc.perform(put("/api/durees")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(duree)))
            .andExpect(status().isBadRequest());

        // Validate the Duree in the database
        List<Duree> dureeList = dureeRepository.findAll();
        assertThat(dureeList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteDuree() throws Exception {
        // Initialize the database
        dureeRepository.saveAndFlush(duree);

        int databaseSizeBeforeDelete = dureeRepository.findAll().size();

        // Delete the duree
        restDureeMockMvc.perform(delete("/api/durees/{id}", duree.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Duree> dureeList = dureeRepository.findAll();
        assertThat(dureeList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Duree.class);
        Duree duree1 = new Duree();
        duree1.setId(1L);
        Duree duree2 = new Duree();
        duree2.setId(duree1.getId());
        assertThat(duree1).isEqualTo(duree2);
        duree2.setId(2L);
        assertThat(duree1).isNotEqualTo(duree2);
        duree1.setId(null);
        assertThat(duree1).isNotEqualTo(duree2);
    }
}
