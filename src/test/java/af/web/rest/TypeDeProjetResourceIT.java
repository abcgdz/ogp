package af.web.rest;

import af.OgpApp;
import af.domain.TypeDeProjet;
import af.repository.TypeDeProjetRepository;
import af.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;

import static af.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link TypeDeProjetResource} REST controller.
 */
@SpringBootTest(classes = OgpApp.class)
public class TypeDeProjetResourceIT {

    private static final String DEFAULT_LIBELLE = "AAAAAAAAAA";
    private static final String UPDATED_LIBELLE = "BBBBBBBBBB";

    private static final Integer DEFAULT_CODE = 1;
    private static final Integer UPDATED_CODE = 2;
    private static final Integer SMALLER_CODE = 1 - 1;

    @Autowired
    private TypeDeProjetRepository typeDeProjetRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restTypeDeProjetMockMvc;

    private TypeDeProjet typeDeProjet;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final TypeDeProjetResource typeDeProjetResource = new TypeDeProjetResource(typeDeProjetRepository);
        this.restTypeDeProjetMockMvc = MockMvcBuilders.standaloneSetup(typeDeProjetResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static TypeDeProjet createEntity(EntityManager em) {
        TypeDeProjet typeDeProjet = new TypeDeProjet()
            .libelle(DEFAULT_LIBELLE)
            .code(DEFAULT_CODE);
        return typeDeProjet;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static TypeDeProjet createUpdatedEntity(EntityManager em) {
        TypeDeProjet typeDeProjet = new TypeDeProjet()
            .libelle(UPDATED_LIBELLE)
            .code(UPDATED_CODE);
        return typeDeProjet;
    }

    @BeforeEach
    public void initTest() {
        typeDeProjet = createEntity(em);
    }

    @Test
    @Transactional
    public void createTypeDeProjet() throws Exception {
        int databaseSizeBeforeCreate = typeDeProjetRepository.findAll().size();

        // Create the TypeDeProjet
        restTypeDeProjetMockMvc.perform(post("/api/type-de-projets")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(typeDeProjet)))
            .andExpect(status().isCreated());

        // Validate the TypeDeProjet in the database
        List<TypeDeProjet> typeDeProjetList = typeDeProjetRepository.findAll();
        assertThat(typeDeProjetList).hasSize(databaseSizeBeforeCreate + 1);
        TypeDeProjet testTypeDeProjet = typeDeProjetList.get(typeDeProjetList.size() - 1);
        assertThat(testTypeDeProjet.getLibelle()).isEqualTo(DEFAULT_LIBELLE);
        assertThat(testTypeDeProjet.getCode()).isEqualTo(DEFAULT_CODE);
    }

    @Test
    @Transactional
    public void createTypeDeProjetWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = typeDeProjetRepository.findAll().size();

        // Create the TypeDeProjet with an existing ID
        typeDeProjet.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restTypeDeProjetMockMvc.perform(post("/api/type-de-projets")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(typeDeProjet)))
            .andExpect(status().isBadRequest());

        // Validate the TypeDeProjet in the database
        List<TypeDeProjet> typeDeProjetList = typeDeProjetRepository.findAll();
        assertThat(typeDeProjetList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllTypeDeProjets() throws Exception {
        // Initialize the database
        typeDeProjetRepository.saveAndFlush(typeDeProjet);

        // Get all the typeDeProjetList
        restTypeDeProjetMockMvc.perform(get("/api/type-de-projets?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(typeDeProjet.getId().intValue())))
            .andExpect(jsonPath("$.[*].libelle").value(hasItem(DEFAULT_LIBELLE.toString())))
            .andExpect(jsonPath("$.[*].code").value(hasItem(DEFAULT_CODE)));
    }
    
    @Test
    @Transactional
    public void getTypeDeProjet() throws Exception {
        // Initialize the database
        typeDeProjetRepository.saveAndFlush(typeDeProjet);

        // Get the typeDeProjet
        restTypeDeProjetMockMvc.perform(get("/api/type-de-projets/{id}", typeDeProjet.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(typeDeProjet.getId().intValue()))
            .andExpect(jsonPath("$.libelle").value(DEFAULT_LIBELLE.toString()))
            .andExpect(jsonPath("$.code").value(DEFAULT_CODE));
    }

    @Test
    @Transactional
    public void getNonExistingTypeDeProjet() throws Exception {
        // Get the typeDeProjet
        restTypeDeProjetMockMvc.perform(get("/api/type-de-projets/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateTypeDeProjet() throws Exception {
        // Initialize the database
        typeDeProjetRepository.saveAndFlush(typeDeProjet);

        int databaseSizeBeforeUpdate = typeDeProjetRepository.findAll().size();

        // Update the typeDeProjet
        TypeDeProjet updatedTypeDeProjet = typeDeProjetRepository.findById(typeDeProjet.getId()).get();
        // Disconnect from session so that the updates on updatedTypeDeProjet are not directly saved in db
        em.detach(updatedTypeDeProjet);
        updatedTypeDeProjet
            .libelle(UPDATED_LIBELLE)
            .code(UPDATED_CODE);

        restTypeDeProjetMockMvc.perform(put("/api/type-de-projets")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedTypeDeProjet)))
            .andExpect(status().isOk());

        // Validate the TypeDeProjet in the database
        List<TypeDeProjet> typeDeProjetList = typeDeProjetRepository.findAll();
        assertThat(typeDeProjetList).hasSize(databaseSizeBeforeUpdate);
        TypeDeProjet testTypeDeProjet = typeDeProjetList.get(typeDeProjetList.size() - 1);
        assertThat(testTypeDeProjet.getLibelle()).isEqualTo(UPDATED_LIBELLE);
        assertThat(testTypeDeProjet.getCode()).isEqualTo(UPDATED_CODE);
    }

    @Test
    @Transactional
    public void updateNonExistingTypeDeProjet() throws Exception {
        int databaseSizeBeforeUpdate = typeDeProjetRepository.findAll().size();

        // Create the TypeDeProjet

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restTypeDeProjetMockMvc.perform(put("/api/type-de-projets")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(typeDeProjet)))
            .andExpect(status().isBadRequest());

        // Validate the TypeDeProjet in the database
        List<TypeDeProjet> typeDeProjetList = typeDeProjetRepository.findAll();
        assertThat(typeDeProjetList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteTypeDeProjet() throws Exception {
        // Initialize the database
        typeDeProjetRepository.saveAndFlush(typeDeProjet);

        int databaseSizeBeforeDelete = typeDeProjetRepository.findAll().size();

        // Delete the typeDeProjet
        restTypeDeProjetMockMvc.perform(delete("/api/type-de-projets/{id}", typeDeProjet.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<TypeDeProjet> typeDeProjetList = typeDeProjetRepository.findAll();
        assertThat(typeDeProjetList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(TypeDeProjet.class);
        TypeDeProjet typeDeProjet1 = new TypeDeProjet();
        typeDeProjet1.setId(1L);
        TypeDeProjet typeDeProjet2 = new TypeDeProjet();
        typeDeProjet2.setId(typeDeProjet1.getId());
        assertThat(typeDeProjet1).isEqualTo(typeDeProjet2);
        typeDeProjet2.setId(2L);
        assertThat(typeDeProjet1).isNotEqualTo(typeDeProjet2);
        typeDeProjet1.setId(null);
        assertThat(typeDeProjet1).isNotEqualTo(typeDeProjet2);
    }
}
