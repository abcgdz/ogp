package af.web.rest;

import af.OgpApp;
import af.domain.Pc;
import af.repository.PcRepository;
import af.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;

import static af.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link PcResource} REST controller.
 */
@SpringBootTest(classes = OgpApp.class)
public class PcResourceIT {

    private static final String DEFAULT_LIBELLE = "AAAAAAAAAA";
    private static final String UPDATED_LIBELLE = "BBBBBBBBBB";

    private static final Integer DEFAULT_CODE = 1;
    private static final Integer UPDATED_CODE = 2;
    private static final Integer SMALLER_CODE = 1 - 1;

    @Autowired
    private PcRepository pcRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restPcMockMvc;

    private Pc pc;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final PcResource pcResource = new PcResource(pcRepository);
        this.restPcMockMvc = MockMvcBuilders.standaloneSetup(pcResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Pc createEntity(EntityManager em) {
        Pc pc = new Pc()
            .libelle(DEFAULT_LIBELLE)
            .code(DEFAULT_CODE);
        return pc;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Pc createUpdatedEntity(EntityManager em) {
        Pc pc = new Pc()
            .libelle(UPDATED_LIBELLE)
            .code(UPDATED_CODE);
        return pc;
    }

    @BeforeEach
    public void initTest() {
        pc = createEntity(em);
    }

    @Test
    @Transactional
    public void createPc() throws Exception {
        int databaseSizeBeforeCreate = pcRepository.findAll().size();

        // Create the Pc
        restPcMockMvc.perform(post("/api/pcs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(pc)))
            .andExpect(status().isCreated());

        // Validate the Pc in the database
        List<Pc> pcList = pcRepository.findAll();
        assertThat(pcList).hasSize(databaseSizeBeforeCreate + 1);
        Pc testPc = pcList.get(pcList.size() - 1);
        assertThat(testPc.getLibelle()).isEqualTo(DEFAULT_LIBELLE);
        assertThat(testPc.getCode()).isEqualTo(DEFAULT_CODE);
    }

    @Test
    @Transactional
    public void createPcWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = pcRepository.findAll().size();

        // Create the Pc with an existing ID
        pc.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restPcMockMvc.perform(post("/api/pcs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(pc)))
            .andExpect(status().isBadRequest());

        // Validate the Pc in the database
        List<Pc> pcList = pcRepository.findAll();
        assertThat(pcList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllPcs() throws Exception {
        // Initialize the database
        pcRepository.saveAndFlush(pc);

        // Get all the pcList
        restPcMockMvc.perform(get("/api/pcs?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(pc.getId().intValue())))
            .andExpect(jsonPath("$.[*].libelle").value(hasItem(DEFAULT_LIBELLE.toString())))
            .andExpect(jsonPath("$.[*].code").value(hasItem(DEFAULT_CODE)));
    }
    
    @Test
    @Transactional
    public void getPc() throws Exception {
        // Initialize the database
        pcRepository.saveAndFlush(pc);

        // Get the pc
        restPcMockMvc.perform(get("/api/pcs/{id}", pc.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(pc.getId().intValue()))
            .andExpect(jsonPath("$.libelle").value(DEFAULT_LIBELLE.toString()))
            .andExpect(jsonPath("$.code").value(DEFAULT_CODE));
    }

    @Test
    @Transactional
    public void getNonExistingPc() throws Exception {
        // Get the pc
        restPcMockMvc.perform(get("/api/pcs/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updatePc() throws Exception {
        // Initialize the database
        pcRepository.saveAndFlush(pc);

        int databaseSizeBeforeUpdate = pcRepository.findAll().size();

        // Update the pc
        Pc updatedPc = pcRepository.findById(pc.getId()).get();
        // Disconnect from session so that the updates on updatedPc are not directly saved in db
        em.detach(updatedPc);
        updatedPc
            .libelle(UPDATED_LIBELLE)
            .code(UPDATED_CODE);

        restPcMockMvc.perform(put("/api/pcs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedPc)))
            .andExpect(status().isOk());

        // Validate the Pc in the database
        List<Pc> pcList = pcRepository.findAll();
        assertThat(pcList).hasSize(databaseSizeBeforeUpdate);
        Pc testPc = pcList.get(pcList.size() - 1);
        assertThat(testPc.getLibelle()).isEqualTo(UPDATED_LIBELLE);
        assertThat(testPc.getCode()).isEqualTo(UPDATED_CODE);
    }

    @Test
    @Transactional
    public void updateNonExistingPc() throws Exception {
        int databaseSizeBeforeUpdate = pcRepository.findAll().size();

        // Create the Pc

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restPcMockMvc.perform(put("/api/pcs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(pc)))
            .andExpect(status().isBadRequest());

        // Validate the Pc in the database
        List<Pc> pcList = pcRepository.findAll();
        assertThat(pcList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deletePc() throws Exception {
        // Initialize the database
        pcRepository.saveAndFlush(pc);

        int databaseSizeBeforeDelete = pcRepository.findAll().size();

        // Delete the pc
        restPcMockMvc.perform(delete("/api/pcs/{id}", pc.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Pc> pcList = pcRepository.findAll();
        assertThat(pcList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Pc.class);
        Pc pc1 = new Pc();
        pc1.setId(1L);
        Pc pc2 = new Pc();
        pc2.setId(pc1.getId());
        assertThat(pc1).isEqualTo(pc2);
        pc2.setId(2L);
        assertThat(pc1).isNotEqualTo(pc2);
        pc1.setId(null);
        assertThat(pc1).isNotEqualTo(pc2);
    }
}
