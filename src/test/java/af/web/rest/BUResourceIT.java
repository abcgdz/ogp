package af.web.rest;

import af.OgpApp;
import af.domain.BU;
import af.repository.BURepository;
import af.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;

import static af.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link BUResource} REST controller.
 */
@SpringBootTest(classes = OgpApp.class)
public class BUResourceIT {

    private static final String DEFAULT_NOM = "AAAAAAAAAA";
    private static final String UPDATED_NOM = "BBBBBBBBBB";

    private static final String DEFAULT_PRENOM = "AAAAAAAAAA";
    private static final String UPDATED_PRENOM = "BBBBBBBBBB";

    @Autowired
    private BURepository bURepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restBUMockMvc;

    private BU bU;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final BUResource bUResource = new BUResource(bURepository);
        this.restBUMockMvc = MockMvcBuilders.standaloneSetup(bUResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static BU createEntity(EntityManager em) {
        BU bU = new BU()
            .nom(DEFAULT_NOM)
            .prenom(DEFAULT_PRENOM);
        return bU;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static BU createUpdatedEntity(EntityManager em) {
        BU bU = new BU()
            .nom(UPDATED_NOM)
            .prenom(UPDATED_PRENOM);
        return bU;
    }

    @BeforeEach
    public void initTest() {
        bU = createEntity(em);
    }

    @Test
    @Transactional
    public void createBU() throws Exception {
        int databaseSizeBeforeCreate = bURepository.findAll().size();

        // Create the BU
        restBUMockMvc.perform(post("/api/bus")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(bU)))
            .andExpect(status().isCreated());

        // Validate the BU in the database
        List<BU> bUList = bURepository.findAll();
        assertThat(bUList).hasSize(databaseSizeBeforeCreate + 1);
        BU testBU = bUList.get(bUList.size() - 1);
        assertThat(testBU.getNom()).isEqualTo(DEFAULT_NOM);
        assertThat(testBU.getPrenom()).isEqualTo(DEFAULT_PRENOM);
    }

    @Test
    @Transactional
    public void createBUWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = bURepository.findAll().size();

        // Create the BU with an existing ID
        bU.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restBUMockMvc.perform(post("/api/bus")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(bU)))
            .andExpect(status().isBadRequest());

        // Validate the BU in the database
        List<BU> bUList = bURepository.findAll();
        assertThat(bUList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllBUS() throws Exception {
        // Initialize the database
        bURepository.saveAndFlush(bU);

        // Get all the bUList
        restBUMockMvc.perform(get("/api/bus?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(bU.getId().intValue())))
            .andExpect(jsonPath("$.[*].nom").value(hasItem(DEFAULT_NOM.toString())))
            .andExpect(jsonPath("$.[*].prenom").value(hasItem(DEFAULT_PRENOM.toString())));
    }
    
    @Test
    @Transactional
    public void getBU() throws Exception {
        // Initialize the database
        bURepository.saveAndFlush(bU);

        // Get the bU
        restBUMockMvc.perform(get("/api/bus/{id}", bU.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(bU.getId().intValue()))
            .andExpect(jsonPath("$.nom").value(DEFAULT_NOM.toString()))
            .andExpect(jsonPath("$.prenom").value(DEFAULT_PRENOM.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingBU() throws Exception {
        // Get the bU
        restBUMockMvc.perform(get("/api/bus/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateBU() throws Exception {
        // Initialize the database
        bURepository.saveAndFlush(bU);

        int databaseSizeBeforeUpdate = bURepository.findAll().size();

        // Update the bU
        BU updatedBU = bURepository.findById(bU.getId()).get();
        // Disconnect from session so that the updates on updatedBU are not directly saved in db
        em.detach(updatedBU);
        updatedBU
            .nom(UPDATED_NOM)
            .prenom(UPDATED_PRENOM);

        restBUMockMvc.perform(put("/api/bus")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedBU)))
            .andExpect(status().isOk());

        // Validate the BU in the database
        List<BU> bUList = bURepository.findAll();
        assertThat(bUList).hasSize(databaseSizeBeforeUpdate);
        BU testBU = bUList.get(bUList.size() - 1);
        assertThat(testBU.getNom()).isEqualTo(UPDATED_NOM);
        assertThat(testBU.getPrenom()).isEqualTo(UPDATED_PRENOM);
    }

    @Test
    @Transactional
    public void updateNonExistingBU() throws Exception {
        int databaseSizeBeforeUpdate = bURepository.findAll().size();

        // Create the BU

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restBUMockMvc.perform(put("/api/bus")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(bU)))
            .andExpect(status().isBadRequest());

        // Validate the BU in the database
        List<BU> bUList = bURepository.findAll();
        assertThat(bUList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteBU() throws Exception {
        // Initialize the database
        bURepository.saveAndFlush(bU);

        int databaseSizeBeforeDelete = bURepository.findAll().size();

        // Delete the bU
        restBUMockMvc.perform(delete("/api/bus/{id}", bU.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<BU> bUList = bURepository.findAll();
        assertThat(bUList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(BU.class);
        BU bU1 = new BU();
        bU1.setId(1L);
        BU bU2 = new BU();
        bU2.setId(bU1.getId());
        assertThat(bU1).isEqualTo(bU2);
        bU2.setId(2L);
        assertThat(bU1).isNotEqualTo(bU2);
        bU1.setId(null);
        assertThat(bU1).isNotEqualTo(bU2);
    }
}
