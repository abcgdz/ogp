package af.web.rest;

import af.OgpApp;
import af.domain.Projet;
import af.repository.ProjetRepository;
import af.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;

import static af.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link ProjetResource} REST controller.
 */
@SpringBootTest(classes = OgpApp.class)
public class ProjetResourceIT {

    private static final String DEFAULT_ACRONYME = "AAAAAAAAAA";
    private static final String UPDATED_ACRONYME = "BBBBBBBBBB";

    private static final Integer DEFAULT_PROBABILITE_GAIN = 1;
    private static final Integer UPDATED_PROBABILITE_GAIN = 2;
    private static final Integer SMALLER_PROBABILITE_GAIN = 1 - 1;

    private static final Integer DEFAULT_CHARGE_JH = 1;
    private static final Integer UPDATED_CHARGE_JH = 2;
    private static final Integer SMALLER_CHARGE_JH = 1 - 1;

    private static final String DEFAULT_MOIS_DE_DEMARRAGE = "AAAAAAAAAA";
    private static final String UPDATED_MOIS_DE_DEMARRAGE = "BBBBBBBBBB";

    private static final String DEFAULT_DUREE = "AAAAAAAAAA";
    private static final String UPDATED_DUREE = "BBBBBBBBBB";

    private static final String DEFAULT_MOIS_DE_CLOTURE = "AAAAAAAAAA";
    private static final String UPDATED_MOIS_DE_CLOTURE = "BBBBBBBBBB";

    @Autowired
    private ProjetRepository projetRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restProjetMockMvc;

    private Projet projet;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ProjetResource projetResource = new ProjetResource(projetRepository);
        this.restProjetMockMvc = MockMvcBuilders.standaloneSetup(projetResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Projet createEntity(EntityManager em) {
        Projet projet = new Projet()
            .acronyme(DEFAULT_ACRONYME)
            .probabiliteGain(DEFAULT_PROBABILITE_GAIN)
            .chargeJh(DEFAULT_CHARGE_JH)
            .moisDeDemarrage(DEFAULT_MOIS_DE_DEMARRAGE)
            .duree(DEFAULT_DUREE)
            .moisDeCloture(DEFAULT_MOIS_DE_CLOTURE);
        return projet;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Projet createUpdatedEntity(EntityManager em) {
        Projet projet = new Projet()
            .acronyme(UPDATED_ACRONYME)
            .probabiliteGain(UPDATED_PROBABILITE_GAIN)
            .chargeJh(UPDATED_CHARGE_JH)
            .moisDeDemarrage(UPDATED_MOIS_DE_DEMARRAGE)
            .duree(UPDATED_DUREE)
            .moisDeCloture(UPDATED_MOIS_DE_CLOTURE);
        return projet;
    }

    @BeforeEach
    public void initTest() {
        projet = createEntity(em);
    }

    @Test
    @Transactional
    public void createProjet() throws Exception {
        int databaseSizeBeforeCreate = projetRepository.findAll().size();

        // Create the Projet
        restProjetMockMvc.perform(post("/api/projets")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(projet)))
            .andExpect(status().isCreated());

        // Validate the Projet in the database
        List<Projet> projetList = projetRepository.findAll();
        assertThat(projetList).hasSize(databaseSizeBeforeCreate + 1);
        Projet testProjet = projetList.get(projetList.size() - 1);
        assertThat(testProjet.getAcronyme()).isEqualTo(DEFAULT_ACRONYME);
        assertThat(testProjet.getProbabiliteGain()).isEqualTo(DEFAULT_PROBABILITE_GAIN);
        assertThat(testProjet.getChargeJh()).isEqualTo(DEFAULT_CHARGE_JH);
        assertThat(testProjet.getMoisDeDemarrage()).isEqualTo(DEFAULT_MOIS_DE_DEMARRAGE);
        assertThat(testProjet.getDuree()).isEqualTo(DEFAULT_DUREE);
        assertThat(testProjet.getMoisDeCloture()).isEqualTo(DEFAULT_MOIS_DE_CLOTURE);
    }

    @Test
    @Transactional
    public void createProjetWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = projetRepository.findAll().size();

        // Create the Projet with an existing ID
        projet.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restProjetMockMvc.perform(post("/api/projets")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(projet)))
            .andExpect(status().isBadRequest());

        // Validate the Projet in the database
        List<Projet> projetList = projetRepository.findAll();
        assertThat(projetList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllProjets() throws Exception {
        // Initialize the database
        projetRepository.saveAndFlush(projet);

        // Get all the projetList
        restProjetMockMvc.perform(get("/api/projets?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(projet.getId().intValue())))
            .andExpect(jsonPath("$.[*].acronyme").value(hasItem(DEFAULT_ACRONYME.toString())))
            .andExpect(jsonPath("$.[*].probabiliteGain").value(hasItem(DEFAULT_PROBABILITE_GAIN)))
            .andExpect(jsonPath("$.[*].chargeJh").value(hasItem(DEFAULT_CHARGE_JH)))
            .andExpect(jsonPath("$.[*].moisDeDemarrage").value(hasItem(DEFAULT_MOIS_DE_DEMARRAGE.toString())))
            .andExpect(jsonPath("$.[*].duree").value(hasItem(DEFAULT_DUREE.toString())))
            .andExpect(jsonPath("$.[*].moisDeCloture").value(hasItem(DEFAULT_MOIS_DE_CLOTURE.toString())));
    }
    
    @Test
    @Transactional
    public void getProjet() throws Exception {
        // Initialize the database
        projetRepository.saveAndFlush(projet);

        // Get the projet
        restProjetMockMvc.perform(get("/api/projets/{id}", projet.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(projet.getId().intValue()))
            .andExpect(jsonPath("$.acronyme").value(DEFAULT_ACRONYME.toString()))
            .andExpect(jsonPath("$.probabiliteGain").value(DEFAULT_PROBABILITE_GAIN))
            .andExpect(jsonPath("$.chargeJh").value(DEFAULT_CHARGE_JH))
            .andExpect(jsonPath("$.moisDeDemarrage").value(DEFAULT_MOIS_DE_DEMARRAGE.toString()))
            .andExpect(jsonPath("$.duree").value(DEFAULT_DUREE.toString()))
            .andExpect(jsonPath("$.moisDeCloture").value(DEFAULT_MOIS_DE_CLOTURE.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingProjet() throws Exception {
        // Get the projet
        restProjetMockMvc.perform(get("/api/projets/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateProjet() throws Exception {
        // Initialize the database
        projetRepository.saveAndFlush(projet);

        int databaseSizeBeforeUpdate = projetRepository.findAll().size();

        // Update the projet
        Projet updatedProjet = projetRepository.findById(projet.getId()).get();
        // Disconnect from session so that the updates on updatedProjet are not directly saved in db
        em.detach(updatedProjet);
        updatedProjet
            .acronyme(UPDATED_ACRONYME)
            .probabiliteGain(UPDATED_PROBABILITE_GAIN)
            .chargeJh(UPDATED_CHARGE_JH)
            .moisDeDemarrage(UPDATED_MOIS_DE_DEMARRAGE)
            .duree(UPDATED_DUREE)
            .moisDeCloture(UPDATED_MOIS_DE_CLOTURE);

        restProjetMockMvc.perform(put("/api/projets")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedProjet)))
            .andExpect(status().isOk());

        // Validate the Projet in the database
        List<Projet> projetList = projetRepository.findAll();
        assertThat(projetList).hasSize(databaseSizeBeforeUpdate);
        Projet testProjet = projetList.get(projetList.size() - 1);
        assertThat(testProjet.getAcronyme()).isEqualTo(UPDATED_ACRONYME);
        assertThat(testProjet.getProbabiliteGain()).isEqualTo(UPDATED_PROBABILITE_GAIN);
        assertThat(testProjet.getChargeJh()).isEqualTo(UPDATED_CHARGE_JH);
        assertThat(testProjet.getMoisDeDemarrage()).isEqualTo(UPDATED_MOIS_DE_DEMARRAGE);
        assertThat(testProjet.getDuree()).isEqualTo(UPDATED_DUREE);
        assertThat(testProjet.getMoisDeCloture()).isEqualTo(UPDATED_MOIS_DE_CLOTURE);
    }

    @Test
    @Transactional
    public void updateNonExistingProjet() throws Exception {
        int databaseSizeBeforeUpdate = projetRepository.findAll().size();

        // Create the Projet

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restProjetMockMvc.perform(put("/api/projets")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(projet)))
            .andExpect(status().isBadRequest());

        // Validate the Projet in the database
        List<Projet> projetList = projetRepository.findAll();
        assertThat(projetList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteProjet() throws Exception {
        // Initialize the database
        projetRepository.saveAndFlush(projet);

        int databaseSizeBeforeDelete = projetRepository.findAll().size();

        // Delete the projet
        restProjetMockMvc.perform(delete("/api/projets/{id}", projet.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Projet> projetList = projetRepository.findAll();
        assertThat(projetList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Projet.class);
        Projet projet1 = new Projet();
        projet1.setId(1L);
        Projet projet2 = new Projet();
        projet2.setId(projet1.getId());
        assertThat(projet1).isEqualTo(projet2);
        projet2.setId(2L);
        assertThat(projet1).isNotEqualTo(projet2);
        projet1.setId(null);
        assertThat(projet1).isNotEqualTo(projet2);
    }
}
