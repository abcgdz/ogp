package af.web.rest;

import af.OgpApp;
import af.domain.DetailPC;
import af.repository.DetailPCRepository;
import af.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;

import static af.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link DetailPCResource} REST controller.
 */
@SpringBootTest(classes = OgpApp.class)
public class DetailPCResourceIT {

    private static final String DEFAULT_LIBELLE = "AAAAAAAAAA";
    private static final String UPDATED_LIBELLE = "BBBBBBBBBB";

    private static final Integer DEFAULT_CODE = 1;
    private static final Integer UPDATED_CODE = 2;
    private static final Integer SMALLER_CODE = 1 - 1;

    private static final String DEFAULT_ROLE = "AAAAAAAAAA";
    private static final String UPDATED_ROLE = "BBBBBBBBBB";

    @Autowired
    private DetailPCRepository detailPCRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restDetailPCMockMvc;

    private DetailPC detailPC;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final DetailPCResource detailPCResource = new DetailPCResource(detailPCRepository);
        this.restDetailPCMockMvc = MockMvcBuilders.standaloneSetup(detailPCResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static DetailPC createEntity(EntityManager em) {
        DetailPC detailPC = new DetailPC()
            .libelle(DEFAULT_LIBELLE)
            .code(DEFAULT_CODE)
            .role(DEFAULT_ROLE);
        return detailPC;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static DetailPC createUpdatedEntity(EntityManager em) {
        DetailPC detailPC = new DetailPC()
            .libelle(UPDATED_LIBELLE)
            .code(UPDATED_CODE)
            .role(UPDATED_ROLE);
        return detailPC;
    }

    @BeforeEach
    public void initTest() {
        detailPC = createEntity(em);
    }

    @Test
    @Transactional
    public void createDetailPC() throws Exception {
        int databaseSizeBeforeCreate = detailPCRepository.findAll().size();

        // Create the DetailPC
        restDetailPCMockMvc.perform(post("/api/detail-pcs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(detailPC)))
            .andExpect(status().isCreated());

        // Validate the DetailPC in the database
        List<DetailPC> detailPCList = detailPCRepository.findAll();
        assertThat(detailPCList).hasSize(databaseSizeBeforeCreate + 1);
        DetailPC testDetailPC = detailPCList.get(detailPCList.size() - 1);
        assertThat(testDetailPC.getLibelle()).isEqualTo(DEFAULT_LIBELLE);
        assertThat(testDetailPC.getCode()).isEqualTo(DEFAULT_CODE);
        assertThat(testDetailPC.getRole()).isEqualTo(DEFAULT_ROLE);
    }

    @Test
    @Transactional
    public void createDetailPCWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = detailPCRepository.findAll().size();

        // Create the DetailPC with an existing ID
        detailPC.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restDetailPCMockMvc.perform(post("/api/detail-pcs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(detailPC)))
            .andExpect(status().isBadRequest());

        // Validate the DetailPC in the database
        List<DetailPC> detailPCList = detailPCRepository.findAll();
        assertThat(detailPCList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllDetailPCS() throws Exception {
        // Initialize the database
        detailPCRepository.saveAndFlush(detailPC);

        // Get all the detailPCList
        restDetailPCMockMvc.perform(get("/api/detail-pcs?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(detailPC.getId().intValue())))
            .andExpect(jsonPath("$.[*].libelle").value(hasItem(DEFAULT_LIBELLE.toString())))
            .andExpect(jsonPath("$.[*].code").value(hasItem(DEFAULT_CODE)))
            .andExpect(jsonPath("$.[*].role").value(hasItem(DEFAULT_ROLE.toString())));
    }
    
    @Test
    @Transactional
    public void getDetailPC() throws Exception {
        // Initialize the database
        detailPCRepository.saveAndFlush(detailPC);

        // Get the detailPC
        restDetailPCMockMvc.perform(get("/api/detail-pcs/{id}", detailPC.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(detailPC.getId().intValue()))
            .andExpect(jsonPath("$.libelle").value(DEFAULT_LIBELLE.toString()))
            .andExpect(jsonPath("$.code").value(DEFAULT_CODE))
            .andExpect(jsonPath("$.role").value(DEFAULT_ROLE.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingDetailPC() throws Exception {
        // Get the detailPC
        restDetailPCMockMvc.perform(get("/api/detail-pcs/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateDetailPC() throws Exception {
        // Initialize the database
        detailPCRepository.saveAndFlush(detailPC);

        int databaseSizeBeforeUpdate = detailPCRepository.findAll().size();

        // Update the detailPC
        DetailPC updatedDetailPC = detailPCRepository.findById(detailPC.getId()).get();
        // Disconnect from session so that the updates on updatedDetailPC are not directly saved in db
        em.detach(updatedDetailPC);
        updatedDetailPC
            .libelle(UPDATED_LIBELLE)
            .code(UPDATED_CODE)
            .role(UPDATED_ROLE);

        restDetailPCMockMvc.perform(put("/api/detail-pcs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedDetailPC)))
            .andExpect(status().isOk());

        // Validate the DetailPC in the database
        List<DetailPC> detailPCList = detailPCRepository.findAll();
        assertThat(detailPCList).hasSize(databaseSizeBeforeUpdate);
        DetailPC testDetailPC = detailPCList.get(detailPCList.size() - 1);
        assertThat(testDetailPC.getLibelle()).isEqualTo(UPDATED_LIBELLE);
        assertThat(testDetailPC.getCode()).isEqualTo(UPDATED_CODE);
        assertThat(testDetailPC.getRole()).isEqualTo(UPDATED_ROLE);
    }

    @Test
    @Transactional
    public void updateNonExistingDetailPC() throws Exception {
        int databaseSizeBeforeUpdate = detailPCRepository.findAll().size();

        // Create the DetailPC

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restDetailPCMockMvc.perform(put("/api/detail-pcs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(detailPC)))
            .andExpect(status().isBadRequest());

        // Validate the DetailPC in the database
        List<DetailPC> detailPCList = detailPCRepository.findAll();
        assertThat(detailPCList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteDetailPC() throws Exception {
        // Initialize the database
        detailPCRepository.saveAndFlush(detailPC);

        int databaseSizeBeforeDelete = detailPCRepository.findAll().size();

        // Delete the detailPC
        restDetailPCMockMvc.perform(delete("/api/detail-pcs/{id}", detailPC.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<DetailPC> detailPCList = detailPCRepository.findAll();
        assertThat(detailPCList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(DetailPC.class);
        DetailPC detailPC1 = new DetailPC();
        detailPC1.setId(1L);
        DetailPC detailPC2 = new DetailPC();
        detailPC2.setId(detailPC1.getId());
        assertThat(detailPC1).isEqualTo(detailPC2);
        detailPC2.setId(2L);
        assertThat(detailPC1).isNotEqualTo(detailPC2);
        detailPC1.setId(null);
        assertThat(detailPC1).isNotEqualTo(detailPC2);
    }
}
